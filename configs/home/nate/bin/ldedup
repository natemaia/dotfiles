#!/bin/bash

# link duplicate files within $1 to $2

prog="${0##*/}"

notdir() { [[ -L "$1" || ! -d "$1" ]]; }
notfile() { [[ -L "$1" || ! -f "$1" ]]; }
error() { printf "error: %s\n\n" "$1"; usage 1; }
usage() { printf "usage: %s <SEARCH_DIR> <LINK_DIR>\n\n\tLink duplicate files in SEARCH_DIR to LINK_DIR\n\n" "$prog"; exit "$1"; }

main()
{
	[[ "$1" == "-h" || "$1" == "--help" ]] && usage 0
	[[ -n "$1" && -d "$1" ]] || error "missing SEARCH_DIR argument"
	[[ -n "$2" && -d "$2" ]] || error "missing LINK_DIR argument"
	local od="$1"
	local id="$2"

	for d in "$od"/*; do
		s="${d##*/}"
		{ notdir "$d" || notdir "$id/$s"; } && continue

		for c in "$d"/*; do
			v="${c##*/}"
			{ notdir "$c" || notdir "$id/$s/$v"; } && continue

			# special case for icon themes, non-symbolic apps/ are linked as full directories
			if [[ $v == "apps" && $s != "symbolic" ]]; then
				rm -rf "$c"
				ln -sfrv "$id/$s/$v" "$d/"
				continue
			fi

			for f in "$c"/*; do
				i="${f##*/}"
				{ notfile "$f" || notfile "$id/$s/$v/$i"; } && continue
				# link when the sha1 hashes are the same
				[[ "$(sha1sum "$id/$s/$v/$i" | cut -d' '  -f1)" == "$(sha1sum "$f" | cut -d' '  -f1)" ]] && ln -sfrv "$id/$s/$v/$i" "$f"
			done
		done
	done

	return 0
}

main "$@"
