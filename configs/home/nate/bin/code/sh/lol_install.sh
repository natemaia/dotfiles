#!/usr/bin/env bash

# Declare packages to be installed
REQ=(alsa-lib gnutls lcms2 lib32-alsa-lib lib32-gnutls
     lib32-lcms2 lib32-libldap lib32-mpg123 libldap
     gendesk icoutils imagemagick alsa-plugins mpg123
     lib32-alsa-plugins lib32-libpulse libpulse openal
     lib32-openal wine-staging winetricks wine-mono wine_gecko
     )

# install wine staging & requirements
for i in "${REQ[@]}"; do
    sudo pacman -S "$i" --needed --noconfirm
done

# Setup wine
WINEPREFIX=$HOME/.wine winetricks d3dx9 vcrun2005 wininet corefonts adobeair ie8

# Lastly try installing
if [[ -e $PWD/LeagueofLegends_NA_Installer_2016_05_13.exe ]]; then
    wine "$PWD/LeagueofLegends_NA_Installer_2016_05_13.exe"
fi

