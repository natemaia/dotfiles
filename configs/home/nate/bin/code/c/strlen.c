#include <stdio.h>


int main(int argc, char *argv[])
{
	int i;

	if (argc < 2 || argc > 3)
		return 1;

	for (i = 0; argv[1][i]; i++)
		;

	printf("%s is %d characters\n", argv[1], i);

	/* return i; */
}
