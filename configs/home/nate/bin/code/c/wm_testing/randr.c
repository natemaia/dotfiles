#define _GNU_SOURCE /* access to vasprintf(3) */
#include <err.h>
#include <stdio.h>
#include <regex.h>
#include <signal.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/wait.h>
#include <xcb/xcb.h>
#include <xcb/randr.h>

#define DBG(fmt, ...) print("%s:%d - " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)

int randrbase;
static int scr_w, scr_h; /* root window size */
static xcb_screen_t *scr; /* the X screen */
static xcb_connection_t *con; /* xcb connection to the X server */
static xcb_window_t root, wmcheck; /* root window and _NET_SUPPORTING_WM_CHECK window */

int initrandr(void);
void initmons(xcb_randr_output_t *outputs, int len, xcb_timestamp_t timestamp);

static void print(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fprintf(stderr, "\n");
}

static Monitor *idtomon(xcb_randr_output_t id)
{
	Monitor *m;

	for (m = mons; m; m = m->next)
		if (m->id == id)
			break;
	return m;
}

static Monitor *initmon(int num, char *name, xcb_randr_output_t id, int x, int y, int w, int h)
{
	Monitor *m;

	DBG("initializing new monitor: %s -- num: %d -- %d,%d - %dx%d", name, num, x, y, w, h);
	m = ecalloc(1, sizeof(Monitor));
	m->x = x;
	m->y = y;
	m->w = w;
	m->h = h;
	m->id = id;
	m->num = num;
	m->name = name;

	return m;
}

Monitor *findclone(xcb_randr_output_t id, int16_t x, int16_t y)
{
    Monitor *m;

    for (m = mons; m; m = m->next)
        if (id != m->id && m->x == x && m->y == y)
            return m;
    return m;
}

int initrandr(void)
{
    int base;
    const xcb_query_extension_reply_t *ext;
    xcb_randr_get_screen_resources_current_reply_t *r;
    xcb_randr_get_screen_resources_current_cookie_t rc;

    ext = xcb_get_extension_data(con, &xcb_randr_id);

    if (!ext->present) {
        warnx("RANDR extension is not available");
        return -1;
    } else {
		rc = xcb_randr_get_screen_resources_current(con, root);
		DBG("getting RANDR screen resources for root window");
		if (!(r = xcb_randr_get_screen_resources_current_reply(con, rc, NULL))) {
			warnx("no RANDR extension available");
			return;
		}
		initmons(xcb_randr_get_screen_resources_current_outputs(r),
				xcb_randr_get_screen_resources_current_outputs_length(r), r->config_timestamp);
		free(r);
	}
	base = ext->first_event;
	DBG("randrbase is %d", base);
	xcb_randr_select_input(con, root, XCB_RANDR_NOTIFY_MASK_SCREEN_CHANGE|XCB_RANDR_NOTIFY_MASK_OUTPUT_CHANGE|
			XCB_RANDR_NOTIFY_MASK_CRTC_CHANGE|XCB_RANDR_NOTIFY_MASK_OUTPUT_PROPERTY);
	xcb_flush(con);

    return base;
}

void initmons(xcb_randr_output_t *outputs, int len, xcb_timestamp_t t)
{
    char *name;
	int i, len, j = 0, changed = 0;
    xcb_randr_get_crtc_info_cookie_t c;
    xcb_randr_get_output_info_reply_t *o;
    xcb_randr_get_output_info_cookie_t oc[len];
    xcb_randr_get_crtc_info_reply_t *crtc = NULL;

    for (i = 0; i < len; i++)
        oc[i] = xcb_randr_get_output_info(con, outputs[i], t);
    for (i = 0; i < len; i++) {
        if ((o = xcb_randr_get_output_info_reply(con, oc[i], NULL)) == NULL)
            continue;
        asprintf(&name, "%.*s", xcb_randr_get_output_info_name_length(o), xcb_randr_get_output_info_name(o));
        DBG("name: %s -- id: %d -- size: %dx%d mm", name, outputs[i], o->mm_width, o->mm_height);

        if (o->crtc != XCB_NONE) {
            c = xcb_randr_get_crtc_info(con, o->crtc, t);
            if ((crtc = xcb_randr_get_crtc_info_reply(con, c, NULL)) == NULL)
                return;
            DBG("CRTC: location: %d,%d -- size: %dx%d -- status: %d", crtc->x, crtc->y, crtc->width, crtc->height, crtc->status);
            if ((clone = findclone(outputs[i], crtc->x, crtc->y)) != NULL) {
                DBG("Monitor %s, id %d is a clone of %s, id %d. Skipping", name, outputs[i], clone->name, clone->id);
                continue;
            } else if (!(m = findmonitor(outputs[i]))) {
                DBG("Monitor not known, adding to list");
                initmon(j, outputs[i], name, crtc->x, crtc->y, crtc->width, crtc->height);
				j++;
			} else {
                DBG("Known monitor. Updating info");
				changed = crtc->x != m->x || crtc->y != m->y || crtc->width != m->w || crtc->height != m->h;
				if (crtc->x != m->x)      m->x = crtc->x;
				if (crtc->y != m->y)      m->y = crtc->y;
				if (crtc->width != m->w)  m->w = crtc->width;
                if (crtc->height != m->h) m->h = crtc->height;
            }
            free(crtc);
        } else {
            if ((m = idtomon(outputs[i]))) {
				DBG("Monitor was used but is now unused");
                /* Check all windows on this monitor and move them to the next or to the first monitor if there is no next */
                freemon(m);
			} else {
				DBG("Monitor is not used");
			}
        }
        free(output);
    }
}

int main(int argc, char *argv[])
{
	if (xcb_connection_has_error((con = xcb_connect(NULL, NULL))))
		errx(1, "error connecting to X");
	if (!(scr = xcb_setup_roots_iterator(xcb_get_setup(con)).data))
		errx(1, "error getting default screen from X connection");
	root = scr->root;
	scr_w = scr->width_in_pixels;
	scr_h = scr->height_in_pixels;
	DBG("initialized root window: %i - size: %dx%d", root, scr_w, scr_h);
	randrbase = initrandr();
	return 0;
}
