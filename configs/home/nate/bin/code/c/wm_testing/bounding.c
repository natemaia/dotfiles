#include <stdio.h>

#define LEN(x) (sizeof(x) / sizeof(*x))

typedef struct Monitor {
	int x, y, w, h;
} Monitor;

Monitor monitors[] = {
	{    0,    0, 3840, 2160 },
	{ 3840,    0, 1920, 1080 },
	{ 3840, 1080, 1920, 1200 }
};

int main(int argc, char *argv[])
{
	int w = 0, h = 0;

	for (int i = 0; i < LEN(monitors); i++) {
		if (monitors[i].x + monitors[i].w > w)
			w = monitors[i].x + monitors[i].w;
		if (monitors[i].y + monitors[i].h > h)
			h = monitors[i].y + monitors[i].h;
	}

	printf("width = %d\nheight = %d\n", w, h);

	return 0;
}
