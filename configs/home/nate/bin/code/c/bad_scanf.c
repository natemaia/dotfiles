/* example of missing the & in scanf() can cause memory problems */
#include <stdio.h>

int main()
{
    int a;
    scanf("%d", a); /* incorrect */
    /* scanf("%d", &a); /1* correct *1/ */
    printf("%d\n", a);
    return 0;
}
