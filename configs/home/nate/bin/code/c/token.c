#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

char *nexttoken(char **src)
{
	size_t n = 0;
	int q, sq = 0;
	char *s, *t, *head, *tail;

	if (!(*src) || !(**src))
		return NULL;

	while (**src && (**src == ' ' || **src == '\t' || **src == '='))
		(*src)++;

	if ((q = **src == '"' || (sq = **src == '\''))) {
		head = *src + 1;
		if (!(tail = strchr(head, sq ? '\'' : '"')))
			return 0;
		if (!sq)
			while (*(tail - 1) == '\\')
				tail = strchr(tail + 1, '"');
	} else {
		head = *src;
		tail = strpbrk(*src, " =\n\t");
	}

	s = t = head;
	while (tail ? s < tail : *s) {
		if (q && !sq && *s == '\\' && *(s + 1) == '"') {
			s++;
		} else {
			n++;
			*t++ = *s++;
		}
	}
	*t = '\0';
	*src = tail ? ++tail : '\0';

	return head;
}

void parse(char *buf)
{
	int n = 0, max = 10;
	char **argv, *tok, *key;

	/* first token is the keyword */
	if (!(key = nexttoken(&buf)))
		return;

	/* tokenize the remaining buffer into an array of char *'s */
	argv = calloc(max, sizeof(char *));
	while ((tok = nexttoken(&buf))) {
		if (n + 1 >= max && !(argv = realloc(argv, (max *= 2) * sizeof(char *))))
			err(1, "unable to reallocate space");
		argv[n++] = tok;
	}
	argv[n] = NULL;

	/* verify the buffer was split correctly */
	fprintf(stderr, "key = %s\n", key);
	for (int j = 0; j < n; j++)
		fprintf(stderr, "argv[%d] = %s\n", j, argv[j]);

	free(argv);
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "error: requires at least one argument to parse\n");
		return 1;
	}

	for (int i = 1; i < argc; i++) {
		fprintf(stderr, "parsing input argument: argv[%d] = \"%s\"\n", i, argv[i]);
		parse(argv[i]);
	}

	return 0;
}
