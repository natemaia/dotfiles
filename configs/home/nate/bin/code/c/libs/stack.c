/* simple stack implementation */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "stack.h"

void stacknew(Stack *s, unsigned long elemsize)
{
	s->elemsize = elemsize;
	s->loglen = 0;
	s->alloclen = 4;
	s->elems = malloc(elemsize * 4);
	assert(s->elems != NULL);
}

void stackdel(Stack *s)
{
	free(s->elems);
}

void stackpop(Stack *s, void *elemaddr)
{
	void *source;

	source = (char *)s->elems + (s->loglen - 1) * s->elemsize;
	memcpy(elemaddr, source, s->elemsize);
	s->loglen--;
}

void stackpush(Stack *s, void *elemaddr)
{
	void *target;

	if (s->loglen == s->alloclen) {
		s->alloclen *= 2;
		s->elems = realloc(s->elems, s->alloclen * s->elemsize);
	}
	target = (char *)s->elems + s->loglen * s->elemsize;
	memcpy(target, elemaddr, s->elemsize);
	s->loglen++;
}

int main(int argc, char *argv[])
{
	/* example using a stack of ints */
	int intarr[3] = { 10, 3, 41 };
	int top;
	Stack intstack;
	stacknew(&intstack, sizeof(int));
	for (int i = 0; i < 3; i++) {
		int tmp = intarr[i];
		stackpush(&intstack, &tmp);
	}
	for (int i = 0; i < 3; i++) {
		stackpop(&intstack, &top);
		printf("%d\n", top);
	}
	stackdel(&intstack);

	/* example of using a stack of strings */
	const char *friends[3] = { "Andrew", "Eric", "Linden" };
	const char *name;
	Stack strstack;
	stacknew(&strstack, sizeof(char *));
	for (int i = 0; i < 3; i++) {
		char *copy = strdup(friends[i]);
		stackpush(&strstack, &copy);
	}
	for (int i = 0; i < 3; i++) {
		stackpop(&strstack, &name);
		printf("%s\n", name);
	}
	stackdel(&strstack);

	return 0;
}
