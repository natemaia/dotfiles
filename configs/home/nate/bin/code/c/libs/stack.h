typedef struct {
	void *elems;
	unsigned long elemsize;
	int loglen;
	int alloclen;
} Stack;

void stackdel(Stack *s);
void stackpop(Stack *s, void *elemaddr);
void stackpush(Stack *s, void *elemaddr);
void stacknew(Stack *s, unsigned long elemsize);
