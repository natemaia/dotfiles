#include <string.h>

/*
 * copy UTF8 strings with multi-byte characters.
 * If src is too long, it does a reverse search
 * (starting at the null terminator) working backward
 * to find the last full UTF8 character that fits into
 * dst. Ensures dst is null terminated.
 */
char *utf8cpy(char* dst, const char* src, size_t dsize)
{
	size_t ssize;
	const char *last;

    if (dsize) {
        ssize = strlen(src); /* number of bytes (not including null) */
        while (ssize >= dsize) {
            last = src + ssize; /* initially, pointing to the null terminator. */
            while (last-- > src) {
				/* initial byte of (potentially) multi-byte char (or null). */
				if ((*last & 0xC0) != 0x80) {
					break;
				}
			}
            ssize = last - src;
        }
        memcpy(dst, src, ssize);
        dst[ssize] = '\0';
    }
    return dst;
}
