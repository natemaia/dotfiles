#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[])
{
	size_t j = 0, n = 0;
	int i, offset = 1;
	char *equal = NULL, *space = NULL, buf[BUFSIZ];

	for (i = 1; i < argc; i++)
		printf("argv[%d]: %s\n", i, argv[i]);

	for (i = 1, j = 0, offset = 1; n + 1 < sizeof(buf) && i < argc; i++, j = 0, offset = 1) {
		if ((space = strchr(argv[i], ' ')) || (space = strchr(argv[i], '\t'))) {
			if (!(equal = strchr(argv[i], '=')) || space < equal)
				buf[n++] = '"';
			offset++;
		}
		while (n + offset + 1 < sizeof(buf) && argv[i][j]) {
			if (argv[i][j] == '"' && offset > 1)
				buf[n++] = '\\';
			buf[n++] = argv[i][j++];
			if (equal && space > equal && buf[n - 1] == '=') {
				buf[n++] = '"';
				equal = NULL;
			}
		}
		if (offset > 1)
			buf[n++] = '"';
		buf[n++] = ' ';
	}
	buf[n - 1] = '\0';

	printf("final buffer: %s\n", buf);

	return 0;
}
