#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	unsigned int i;

	for (i = 1; i < argc; i++) {
		fprintf(stderr, "string argument: %s\n", argv[i]);
		fprintf(stderr, "atoi digit argument: %d\n", atoi(argv[i]));
		fprintf(stderr, "strtol digit argument: %ld\n", strtol(argv[i], NULL, 0));
		if (*argv[i] == '#')
			fprintf(stderr, "strtol hex digit argument: %ld\n", strtol(++argv[i], NULL, 16));
	}
}
