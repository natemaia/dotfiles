/* Generates an X event, like keypress/mouseclick/move/etc like a little man in your computer. :) */

/* gcc -g -Wall -Wextra fakit.c -o fakit -lxcb -lxcb-xtest -lxcb-keysyms -lxcb-util */

#include <unistd.h>
#include <xcb/xtest.h>
#include <xcb/xcb_util.h>
#include <xcb/xcb_keysyms.h>

#define XK_Alt_L  0xffe9 // Left Alt
#define XK_w      0x77   // w key
#define XK_s      0x73   // s key
#define XK_d      0x64   // d key
#define XK_b      0x62   // b key
#define XK_v      0x76   // v key

xcb_key_symbols_t *syms = NULL;


/* Elden Ring zip */
int main(void)
{
	/* Primary timings for the setup (in frames)
	 * "waitframes" is the initial block time before starting to walk
	 * "walkframes" is how long to walk and usually has to be between about 4 and 10 frames
	 * The timings have to add up to around 139 frames, like 134/5, 133/6, 132/7..
	 */
	unsigned int waitframes = 133, walkframes = 6;

	/* How many cycles to offset the zip Larger offset = shorter zips */
	unsigned int cycleoffset = 0;

	/* Fine tuning for timings for testing (in ms) */
	unsigned int waitoffsetms = 0;
	unsigned int walkoffsetms = 0;


	int rmb;
	xcb_keycode_t alt, w;
	xcb_connection_t *c = NULL;
	
	/* bail on bad connection */
	if ((c = xcb_connect(NULL, NULL)) == NULL) return 1;

	/* prep for keysym -> keycode conversion */
	syms = xcb_key_symbols_alloc(c);

	/* right mouse button and alt key */
	rmb = XCB_BUTTON_INDEX_3;
	w = *xcb_key_symbols_get_keycode(syms, XK_w);
	alt = *xcb_key_symbols_get_keycode(syms, XK_Alt_L);

	/* press the alt key and right mouse button then wait for the set amount of time (waitframes) */
	xcb_test_fake_input(c, XCB_BUTTON_PRESS, rmb, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_PRESS, alt, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);
	usleep(((waitframes + 120 * cycleoffset) * 1000 / 60 + waitoffsetms) * 1000);

	/* press the 'w' key and wait for the set amount of time (walkframes) */
	xcb_test_fake_input(c, XCB_KEY_PRESS, w, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);
	usleep((walkframes * 1000 / 60 + walkoffsetms) * 1000);

	/* release the 'w' key */
	xcb_test_fake_input(c, XCB_KEY_RELEASE, w, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);

	/* release the alt key and right mouse button after a wait */
	usleep((30 * 1000 / 60) * 1000);
	xcb_test_fake_input(c, XCB_KEY_RELEASE, alt, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_BUTTON_RELEASE, rmb, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);

	/* Clean up */
	xcb_key_symbols_free(syms);
	xcb_disconnect(c);

	return 0;
}

/* DS2 SOTFS parry walk */
int main1(void)
{
	xcb_keycode_t alt, s, d, v, b;
	xcb_connection_t *c = NULL;
	
	/* bail on bad connection */
	if ((c = xcb_connect(NULL, NULL)) == NULL) return 1;

	/* prep for keysym -> keycode conversion */
	syms = xcb_key_symbols_alloc(c);

	s = *xcb_key_symbols_get_keycode(syms, XK_s);
	d = *xcb_key_symbols_get_keycode(syms, XK_d);
	v = *xcb_key_symbols_get_keycode(syms, XK_v);
	b = *xcb_key_symbols_get_keycode(syms, XK_b);
	alt = *xcb_key_symbols_get_keycode(syms, XK_Alt_L);

	xcb_test_fake_input(c, XCB_KEY_PRESS, alt, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_PRESS, s, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_PRESS, d, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);
	usleep(500000); // 0.25s

	xcb_test_fake_input(c, XCB_KEY_PRESS, b, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_PRESS, v, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);
	usleep(300000); // 0.3s

	xcb_test_fake_input(c, XCB_KEY_RELEASE, alt, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_RELEASE, s, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_RELEASE, d, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_RELEASE, v, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_test_fake_input(c, XCB_KEY_RELEASE, b, 0, XCB_WINDOW_NONE, 0, 0, 0);
	xcb_aux_sync(c);

	xcb_key_symbols_free(syms);
	xcb_disconnect(c);

	return 0;
}
