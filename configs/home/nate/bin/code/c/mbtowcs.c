/* find the length of the multibyte string and store the characters in the wchar_t array */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#define SIZE 30

int main(void)
{

	int len;
	mbstate_t s, ss;
	wchar_t wcs[SIZE], wcs2[SIZE];
	char mbs[SIZE] = "abcd", mbs2[SIZE] = "κοινό.txt";
	char *ptr = mbs;

	wcs[0] = L'\0';
	len = mbsrtowcs(wcs, (const char**)&ptr, SIZE, NULL); // NOLINT
	wcs[len] = L'\0';

	printf("len:   %d\n",  len);
	printf("wcs:   %ls\n", wcs);
	printf("mbs:   %s\n",  mbs);
	printf("&mbs:  %p\n",  mbs);
	printf("&ptr:  %p\n",  ptr);

	ptr = mbs2;
	wcs2[0] = L'\0';
	len = mbsrtowcs(wcs2, (const char**)&ptr, SIZE, &s); // NOLINT
	if (len < 0) {
		perror("mbsrtowcs");
		/* return EXIT_FAILURE; */
	} else {
		wcs2[len] = L'\0';
	}

	printf("state: %d\n", s.__count);
	printf("state: %u\n", s.__value.__wch);
	printf("state: %s\n", s.__value.__wchb);
	printf("len:   %d\n",  len);
	printf("strlen:   %zu\n",  strlen(mbs2));
	/* printf("wcslen:   %lu\n",  wcslen(mbs2)); */
	/* printf("wcslen:   %lu\n",  wcslen(wcs2)); */
	printf("wcs2:  %ls\n", wcs2);
	printf("wcs2 raw:  %s\n", "κοινό.txt");
	printf("mbs2:  %s\n",  mbs2);
	printf("&mbs2: %p\n",  mbs2);
	printf("&ptr:  %p\n",  ptr);
}
