#include <stdio.h>

#define FLOATING(c)    (((c).state & floating))
#define FULLSCREEN(c)  (((c).state & fullscreen) && !((c).state & fakefull))

enum States {
	none = 0,
	fakefull = (1 << 1),
	fixed = (1 << 2),
	floating = (1 << 3),
	fullscreen = (1 << 4),
	noborder = (1 << 5),
	noinput = (1 << 6),
	sticky = (1 << 7),
	urgent = (1 << 8),
};

typedef struct Client Client;

struct Client {
	int x, y, w, h, bw, hoff, depth;
	int old_x, old_y, old_w, old_h, old_bw;
	int max_w, max_h, min_w, min_h;
	int base_w, base_h, increment_w, increment_h;
	float min_aspect, max_aspect;

	unsigned int state, old_state;
};

int main()
{
	Client c;

	c.state = none;
	c.old_state = none;
	printf("state before: %d\n", c.state);
	printf("FLOATING: %d\n", FLOATING(c));
	printf("FULLSCREEN: %d\n\n", FULLSCREEN(c));

	c.old_state |= c.state;
	c.state |= (floating | fullscreen);
	printf("state after: %d\n", c.state);
	printf("FLOATING: %d\n", FLOATING(c));
	printf("FULLSCREEN: %d\n\n", FULLSCREEN(c));

	c.state = c.old_state;
	printf("state restored: %d\n", c.state);
	printf("FLOATING: %d\n", FLOATING(c));
	printf("FULLSCREEN: %d\n", FULLSCREEN(c));
	return 0;
}
