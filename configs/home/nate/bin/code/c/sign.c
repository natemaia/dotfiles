#include <stdio.h>

int main(void)
{
	
	int j = 1;
	unsigned int i = 1;

	printf("signed: %d\n", j);
	printf("unsigned: %d\n", i);

	j = -1;
	i = j;
	printf("after -1 -- signed: %d\n", i);
	printf("after -1 -- unsigned: %d\n", i);

	if (j == -1) {
		i = -1;
		printf("unsigned: %d\n", i);
	}

	return 0;
}
