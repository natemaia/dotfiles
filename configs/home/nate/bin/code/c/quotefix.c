
#include <err.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>

#include <ctype.h>
#include <string.h>
#include <sys/types.h>

#ifndef VERSION
#define VERSION "0.3"
#endif

size_t strlcat(char *dst, const char *src, size_t size)
{
	size_t n = size, dlen;
	const char *odst = dst;
	const char *osrc = src;

	while (n-- != 0 && *dst != '\0')
		dst++;
	dlen = dst - odst;
	n = size - dlen;

	if (n-- == 0)
		return dlen + strlen(src);
	while (*src != '\0') {
		if (n != 0) {
			*dst++ = *src;
			n--;
		}
		src++;
	}
	*dst = '\0';

	return dlen + (src - osrc);
}

size_t strlcpy(char *dst, const char *src, size_t size)
{
	size_t n = size;
	const char *osrc = src;

	if (n != 0)
		while (--n != 0)
			if ((*dst++ = *src++) == '\0')
				break;
	if (n == 0) {
		if (size != 0)
			*dst = '\0';
		while (*src++);
	}
	return src - osrc - 1;
}

int strqetok(char **src, char *dst, size_t size)
{
	size_t n = 0;
    int q, sq = 0;
    char *s, *head, *tail;

    while (**src && (isspace(**src) || **src == '='))
		(*src)++;

    if ((q = **src == '"' || (sq = **src == '\''))) {
        head = *src + 1;
        if (!(tail = strchr(head, sq ? '\'' : '"')))
			return 0;
		if (!sq)
			while (*(tail - 1) == '\\')
				tail = strchr(tail + 1, '"');
    } else {
        head = *src;
        tail = strpbrk(*src, " =\t\n");
    }

    s = head;
    while (n + 1 < size && tail ? s < tail : *s)
        if (q && !sq && *s == '\\' && *(s + 1) == '"')
            s++;
        else {
            n++;
            *dst++ = *s++;
        }
    *dst = '\0';
	*src = tail ? ++tail : '\0';

    return n || q;
}

int main(int argc, char *argv[])
{
	size_t j = 0, n = 0;
	int i, r = 0, offs = 1;
	char *eq = NULL, *sp = NULL, buf[BUFSIZ];

	if (argc == 1 || !strcmp(argv[1], "-h"))
		errx(argc == 1 ? 1 : 0, "usage: %s [-hv] command", argv[0]);
	if (!strcmp(argv[1], "-v"))
		errx(0, "%s "VERSION, argv[0]);

	for (i = 1, j = 0, offs = 1; n + 1 < sizeof(buf) && i < argc; i++, j = 0, offs = 1) {
		if ((sp = strchr(argv[i], ' ')) || (sp = strchr(argv[i], '\t'))) {
			if (!(eq = strchr(argv[i], '=')) || sp < eq) /* no equal found or equal is part of the quoted string */
				buf[n++] = '"';
			offs++;
		}
		while (n + offs < sizeof(buf) && argv[i][j]) {
			buf[n++] = argv[i][j++];
			if (eq && sp > eq && buf[n - 1] == '=') {
				buf[n++] = '"';
				eq = NULL;
			}
		}
		if (offs > 1)
			buf[n++] = '"';
		buf[n++] = ' ';
	}
	buf[n - 1] = '\0';
	fprintf(stderr, "finished buffer: %s", buf);
	return r;
}
