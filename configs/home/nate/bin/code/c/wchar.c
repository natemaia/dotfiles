#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>

#define LIMIT  0xFF

int main(void)
{
	int wc, ch;

	if (setlocale(LC_ALL, "") == NULL) {
		printf("Locale could not be loaded\n");
		return EXIT_FAILURE;
	}

	for (ch = 0; ch <= LIMIT; ++ch) {
		if ((wc = btowc(ch)) == WEOF)
			printf("%#04x is not a one-byte multibyte character\n", ch);
		else
			printf("%#04x has wide character representation: %#06x\n", ch, wc);
	}

	if ((wc = btowc(EOF)) == WEOF)
		printf("The character is EOF.\n");
	else
		printf("EOF has wide character representation: %#06x\n", wc);

	return EXIT_SUCCESS;
}
/***********************************************************************
  If the locale is bound to SBCS, the output should be similar to:
  0000 has wide character representation: 000000
  0x01 has wide character representation: 0x0001
  ...
  0xfe has wide character representation: 0x00fe
  0xff has wide character representation: 0x00ff
  The character is EOF.
 ************************************************************************/
