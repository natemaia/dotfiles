/* do the blending/multiplication required for X colours and transparency */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
 * separate and multiply each channel by the alpha channel
 * then divide by 255 to get the blended value for each.
 * len should be the number of characters the string contained
 * in order to differentiate 0x00ffffff from 0xffffff
 */
unsigned int blend(unsigned int argb, int len)
{
	unsigned short a, r, g, b;

	if (len == 6) /* no alpha channel is opaque */
		return (argb | 0xff000000);
	if ((a = ((argb & 0xff000000) >> 24)) && a < 0xff) { /* non-zero, non-opaque alpha */
		r = (((argb & 0xff0000) >> 16) * a) / 255;
		g = (((argb & 0xff00) >> 8) * a) / 255;
		b = (((argb & 0xff) >> 0) * a) / 255;
		return (a << 24 | r << 16 | g << 8 | b << 0);
	}
	return argb; /* 0x00 alpha (transparent) */
}

int main(int argc, char *argv[])
{
	char *end;
	int i, len = 0;
	unsigned int argb;

	for (i = 1; i < argc; i++) {
		len = strlen(argv[i]);
		if (argv[i][0] == '#' && len >= 7 && len <= 9) {
			len--;
			argb = strtoul(argv[i] + 1, &end, 16);
		} else {
			if (argv[i][0] == '0' && argv[i][1] == 'x')
				len -= 2;
			argb = strtoul(argv[i], &end, 16);
		}
		if (argb <= 0xffffffff && *end == '\0')
			printf("%s -> blend(0x%08x, %d) -> 0x%08x\n", argv[i], argb, len, blend(argb, len));
	}

	return 0;
}
