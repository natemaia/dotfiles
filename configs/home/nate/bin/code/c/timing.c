#include <stdio.h>
#include <time.h>

#define NUM_TESTS 1000

void func(void)
{
	/* do something */
}

int main(int argc, char *argv[])
{
	int i = NUM_TESTS;
	clock_t t = clock();

	while (i--) func();

	fprintf(stderr, "func() took %f seconds on average over %d runs\n",
			((double)(clock() - t) / CLOCKS_PER_SEC) / NUM_TESTS, NUM_TESTS);
}
