#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>
#include <fcntl.h>

#include <sys/wait.h>
#include <sys/stat.h>


struct renaming {
	char *src;
	char *dst;
	struct renaming *next;
};

int tfd = -1;
char tpath[] = "/tmp/cvmv.XXXXXX";

static void error(int eval, int err, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if (err)
		fprintf(stderr, ": %s\n", strerror(err));
	else
		fputc('\n', stderr);

	if (eval != 0)
		exit(eval);
}

static void *erealloc(void *ptr, size_t size)
{
	ptr = realloc(ptr, size);
	if (ptr == NULL)
		error(1, errno, NULL);
	return ptr;
}

static int call(char *const argv[], int fd[3], int *status)
{
	int err = 0;
	pid_t pid = fork();

	if (pid < 0) {
		err = -1;
	} else if (pid > 0) {
		while ((err = waitpid(pid, status, 0)) == -1 && errno == EINTR);
	} else {
		if (fd != NULL) {
			dup2(fd[0], 0);
			dup2(fd[1], 1);
			dup2(fd[2], 2);
		}
		execvp(argv[0], argv);
		error(1, errno, "exec: %s", argv[0]);
	}
	return err;
}

static int ecall(char *const argv[], int fd[3])
{
	int status;

	if (call(argv, fd, &status) == -1)
		error(1, errno, "call: %s", argv[0]);
	return status;
}

static void cleanup()
{
	if (tfd >= 0)
		unlink(tpath);
}

static char *estrdup(char *s)
{
	char *p = strdup(s);

	if (p == NULL)
		error(1, errno, NULL);
	return p;
}

static int mkdirs(char *path)
{
	char c, *s = path;
	struct stat st;

	while (*s != '\0') {
		if (*s == '/') {
			s++;
			continue;
		}
		for (; *s != '\0' && *s != '/'; s++);
		c = *s;
		*s = '\0';
		if (mkdir(path, 0755) == -1)
			if (errno != EEXIST || stat(path, &st) == -1 || !S_ISDIR(st.st_mode))
				return -1;
		*s = c;
	}
	return 0;
}

static int rmdirs(char *path)
{
	char c, *s = path + strlen(path);

	while (s != path) {
		if (s[-1] == '/') {
			s--;
			continue;
		}
		c = *s;
		*s = '\0';
		if (rmdir(path) == -1)
			return -1;
		for (*s-- = c; s != path && *s != '/'; s--);
	}
	return 0;
}

static int rm(const char *path)
{
	struct stat st;

	if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
		return rmdir(path);
	else
		return unlink(path);
}

static void mk(char *src, char *dst, struct renaming **head)
{
	int cycle = 0;
	struct renaming **ins = NULL, **p, *r = erealloc(NULL, sizeof(*r));

	for (p = head; *p != NULL; p = &(*p)->next) {
		if (ins == NULL && strcmp((*p)->dst, src) == 0)
			ins = p;
		if (ins != NULL && strcmp((*p)->src, dst) == 0)
			cycle = 1;
	}
	if (ins == NULL)
		ins = p;
	r->src = src;
	r->dst = dst;
	r->next = *ins;
	*ins = r;
	if (cycle) {
		size_t n = strlen(src) + 16;
		char *tmp = erealloc(NULL, n);
		unsigned int i = 0;
		do {
			snprintf(tmp, n, "%s.%u", src, i++);
		} while (access(tmp, F_OK) == 0);
		mk(tmp, dst, head);
		r->dst = tmp;
	}
}

int marked_rename(char *path, char *editor)
{
	int i;
	FILE *tf;
	char *editor[] = { NULL, tpath, NULL };
	char *dst = NULL, *s;
	size_t n, size = 0;
	struct renaming *head = NULL, *r;

	if ((editor[0] = getenv("EDITOR")) == NULL || *editor[0] == '\0')
		error(1, 0, "EDITOR not set");

	if ((tfd = mkstemp(tpath)) < 0 || (tf = fdopen(tfd, "r+")) == NULL)
		error(1, errno, "%s", tpath);

	for (i = 1; i < argc; i++)
		fprintf(tf, "%s\n", argv[i]);
	fclose(tf);

	if (ecall(editor, NULL) != 0)
		return 1;

	if ((tf = fopen(tpath, "r")) == NULL)
		error(1, errno, "%s", tpath);

	for (i = 1; i < argc && (n = getline(&dst, &size, tf)) != -1; i++) {
		if (dst[n-1] == '\n')
			dst[--n] = '\0';
		if (strcmp(argv[i], dst) == 0)
			continue;
		mk(argv[i], estrdup(dst), &head);
	}
	fclose(tf);

	for (r = head; r != NULL; r = r->next) {
		if ((s = strrchr(r->dst, '/'))) {
			if ((n = s - r->dst) >= size)
				dst = erealloc(dst, size = n+1);
			memcpy(dst, r->dst, n);
			dst[n] = '\0';
			if (mkdirs(dst) == -1)
				continue;
		}
		if (*r->dst) {
			if (rename(r->src, r->dst))
				error(0, errno, "%s -> %s", r->src, r->dst);
		} else {
			if (rm(r->src) == -1)
				error(0, errno, "%s", r->src);
		}
	}

	for (r = head; r != NULL; r = r->next) {
		if ((s = strrchr(r->src, '/')) != NULL) {
			*s = '\0';
			rmdirs(r->src);
		}
	}

	return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
	int i;
	FILE *tf;
	char *editor[] = { NULL, tpath, NULL };
	char *dst = NULL, *s;
	size_t n, size = 0;
	struct renaming *head = NULL, *r;

	if (argc < 2)
		return 1;
	atexit(cleanup);

	if ((editor[0] = getenv("EDITOR")) == NULL || *editor[0] == '\0')
		error(1, 0, "EDITOR not set");

	if ((tfd = mkstemp(tpath)) < 0 || (tf = fdopen(tfd, "r+")) == NULL)
		error(1, errno, "%s", tpath);

	for (i = 1; i < argc; i++)
		fprintf(tf, "%s\n", argv[i]);
	fclose(tf);

	if (ecall(editor, NULL) != 0)
		return 1;

	if ((tf = fopen(tpath, "r")) == NULL)
		error(1, errno, "%s", tpath);

	for (i = 1; i < argc && (n = getline(&dst, &size, tf)) != -1; i++) {
		if (dst[n-1] == '\n')
			dst[--n] = '\0';
		if (strcmp(argv[i], dst) == 0)
			continue;
		mk(argv[i], estrdup(dst), &head);
	}
	fclose(tf);

	for (r = head; r != NULL; r = r->next) {
		if (access(r->dst, F_OK) == 0) {
			error(0, EEXIST, "%s -> %s", r->src, r->dst);
			continue;
		}
		if ((s = strrchr(r->dst, '/')) != NULL) {
			if ((n = s - r->dst) >= size)
				dst = erealloc(dst, size = n+1);
			memcpy(dst, r->dst, n);
			dst[n] = '\0';
			if (mkdirs(dst) == -1) {
				error(0, errno, "%s -> %s: %s", r->src, r->dst, dst);
				continue;
			}
		}
		if (r->dst[0] != '\0') {
			if (rename(r->src, r->dst) == -1)
				error(0, errno, "%s -> %s", r->src, r->dst);
		} else {
			if (rm(r->src) == -1)
				error(0, errno, "%s", r->src);
		}
	}

	for (r = head; r != NULL; r = r->next) {
		if ((s = strrchr(r->src, '/')) != NULL) {
			*s = '\0';
			rmdirs(r->src);
		}
	}

	return EXIT_SUCCESS;
}
