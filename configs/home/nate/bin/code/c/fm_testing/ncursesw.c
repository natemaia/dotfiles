#include <ncurses.h>

typedef struct Border {
	chtype left, right, top, bot, tleft, tright, bleft, bright;
} Border;

typedef struct Window {
	int x, y, h, w;
	Border bd;
} Window;

void windbg(Window *win)
{
	mvprintw(win->y + 2, win->x + 2, "x = %d w = %d", win->x, win->w);
}

void wininit(Window *win, int x, int y, int w, int h)
{
	win->h = h;
	win->w = w;
	win->x = x;
	win->y = y;
	win->bd.left   = ACS_VLINE;
	win->bd.right  = ACS_VLINE;
	win->bd.top    = ACS_HLINE;
	win->bd.bot    = ACS_HLINE;
	win->bd.tleft  = ACS_ULCORNER;
	win->bd.tright = ACS_URCORNER;
	win->bd.bleft  = ACS_LLCORNER;
	win->bd.bright = ACS_LRCORNER;
}

void draw_box(Window *win)
{
	mvaddch(win->y, win->x, win->bd.tleft);                         /* top left corner */
	mvaddch(win->y, win->x + win->w, win->bd.tright);               /* top right corner */
	mvaddch(win->y + win->h, win->x, win->bd.bleft);                /* bottom left corner */
	mvaddch(win->y + win->h, win->x + win->w, win->bd.bright);      /* bottom right corner */
	mvhline(win->y, win->x + 1, win->bd.top, win->w - 1);           /* top side  */
	mvhline(win->y + win->h, win->x + 1, win->bd.bot, win->w - 1);  /* bottom side */
	mvvline(win->y + 1, win->x, win->bd.left, win->h -1);           /* left side */
	mvvline(win->y + 1, win->x + win->w, win->bd.right, win->h -1); /* right side */
}

void refresh2(Window *win1, Window *win2)
{
	clear();
	windbg(win2);
	windbg(win1);
	draw_box(win2);
	draw_box(win1);
	refresh();
}

int main(int argc, char *argv[])
{
	int ch;
	Window win1, win2;

	initscr();
	cbreak();
	noecho();
	keypad(stdscr, 0);
	curs_set(0);
	wininit(&win1, 0, 0, COLS / 2, LINES - 1);
	wininit(&win2, win1.w, 0, COLS - 1 - win1.w, LINES - 1);
	refresh2(&win1, &win2);

	while ((ch = getch()) != 'q') {
		switch (ch) {
			case KEY_LEFT:
			case 'h':
				if (win1.w > 18) {
					--win1.w;
					++win2.w;
					win2.x = win1.w;
					refresh2(&win1, &win2);
				}
				break;
			case KEY_RIGHT:
			case 'l':
				if (win1.w < COLS - 18) {
					++win1.w;
					--win2.w;
					win2.x = win1.w;
					refresh2(&win1, &win2);
				}
				break;
		}
	}
	endwin();
	return 0;
}
