#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int i;
	char *s, *f;

	for (i = 1; i < argc; i++) {
		if (strrchr(argv[i]+1, '/') != NULL)
			printf("argv[i] is not a root path\n");
		else
			printf("argv[i] is a root path\n");
	}

	return 0;
}
