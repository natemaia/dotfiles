#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>


int main(int argc, char *argv[])
{
	int fd;
	char *file = "/home/nate/bin/code/c/fm_testing/testfile";

	if ((fd = open(file, O_RDWR|O_CREAT, 0666)) == -1) {
		fprintf(stderr, "failed to create file %s\n", file);
		exit(1);
	}
	close(fd);

	return 0;
}
