#include <errno.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

int xmkdir(char *path)
{
	size_t len;
	char tmp[PATH_MAX], *p = NUL;

	snprintf(tmp, sizeof(tmp),"%s",path);
	len = strlen(tmp);

	if (tmp[len -1] == '/')
		tmp[len -1] = '\0';
	for (p = tmp + 1; *p; p++)
		if (*p == '/') {
			*p = '\0';
			if (mkdir(tmp, 0755) < 0 && errno != EEXIST)
				return -1;
			*p = '/';
		}
	if (mkdir(tmp, 0755) < 0 && errno != EEXIST)
		return -1;
	return 0;
}

int main(int argc, char *argv[])
{
	if (argc > 1) {
		if (mkdirs(argv[1]))
			fprintf(stderr, "error: %s\n", strerror(errno));
	} else
		return 1;
	return 0;
}
