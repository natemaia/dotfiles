#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
mkquotedpath(char *path, char *name, char *out)
{
	*out++ = ' ';
	*out++ = '"';
	while (*path != '\0') {
		if (*path == '"')
			*out++ = '\\';
		*out++ = *path++;
	}
	*out++ = '/';
	while (*name != '\0') {
		if (*name == '"')
			*out++ = '\\';
		*out++ = *name++;
	}
	*out++ = '"';
}

void run(char *path, char *name, char *out, char *head)
{
	int i;
	char *names[] = { "mike", "eric", "andrew", "nate", NULL };

	while (*head != '\0')
		*out++ = *head++;
	for (i = 0; i < 4; i++)
		mkquotedpath(path, names[i], out + strlen(out));
	out[strlen(out)+1] = '\0';
}

int
main(int argc, char *argv[])
{
	char cmd[BUFSIZ];
	char path[] = "/path/to";
	char name[] = "file";
	char head[] = "mkdir -p";

	/* strcpy(cmd, head); */
	/* mkquotedpath(path, name, cmd + strlen(cmd)); */
	/* printf("cmd is: %s\n", cmd); */

	run(path, name, cmd, head);
	printf("cmd is: %s\n", cmd);

	return 0;
}
