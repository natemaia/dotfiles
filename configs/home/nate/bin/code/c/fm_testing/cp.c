#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

void oops(char *s1, char *s2)
{
	fprintf(stderr, "error: %s ", s1);
	perror(s2);
	exit(1);
}

int dostat(char *filename)
{
	struct stat fileInfo;

	if (stat(filename, &fileInfo) >= 0 && S_ISREG(fileInfo.st_mode))
		return 1;
	return 0;
}

int copyfile(char *source, char *destination)
{
	int ifd, ofd, nchars;
	char buf[BUFSIZ];

	if ((ifd = open(source, O_RDONLY)) == -1)
		oops("Cannot open ", source);
	if ((ofd = creat(destination, 0644)) == -1) {
		close(ifd);
		oops("Cannot creat ", destination);
	}
	while ((n_chars = read(in_fd, buf, BUFFERSIZE)) > 0) {
		if (write(out_fd, buf, n_chars) != n_chars)
			oops("Write error to ", destination);
		if (n_chars == -1)
			oops("Read error from ", source);
	}
	if (close(in_fd) == -1 || close(out_fd) == -1)
		oops("Error closing files", "");
	return 1;
}

int copydir(char *source, char *destination)
{
	DIR *dir_ptr = NULL;
	struct dirent *direntp;
	char tempDest[PATH_MAX];
	char tempSrc[PATH_MAX];

	strcat(destination, "/");
	strcat(source, "/");
	strcpy(tempDest, destination);
	strcpy(tempSrc, source);

	struct stat fileinfo;

	if ((dir_ptr = opendir(source)) == NULL) {
		fprintf(stderr, "cp1: cannot open %s for copying\n", source);
		return 0;
	}
	while ((direntp = readdir(dir_ptr))) {
		if (dostat(direntp->d_name)) {
			strcat(tempDest, direntp->d_name);
			strcat(tempSrc, direntp->d_name);
			copyfile(tempSrc, tempDest);
			strcpy(tempDest, destination);
			strcpy(tempSrc, source);
		}
	}
	closedir(dir_ptr);
	return 1;
}

int main(int ac, char *av[])
{
	int i;
	char dest[PATH_MAX], src[PATH_MAX];

	if (ac != 3) {
		fprintf(stderr, "usage: %s source destination\n", *av);
		exit(1);
	}

	strcpy(src, av[1]);
	strcpy(dest, av[2]);

	if (src[0] != '/' && dest[0] != '/') {
		copyfile(src, dest);
	else if (src[0] != '/' && dest[0] == '/') {
		for (i = 1; i <= strlen(dest); i++)
			dest[i-1] = dest[i];
		strcat(dest, "/");
		strcat(dest, src);
		copyfile(src, dest);
	} else if (src[0] == '/' && dest[0] == '/') {
		for (i = 1; i <= strlen(dest); i++)
			dest[i-1] = dest[i];
		for (i = 1; i <= strlen(src); i++)
			src[i-1] = src[i];
		copydir(src, dest);
	} else {
		fprintf(stderr, "usage: %s src dest\n", av[0]);
		exit(1);
	}
}

