#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

int srclen, dstlen;

size_t strlcat(char *dst, const char *src, size_t size)
{
	const char *odst = dst;
	const char *osrc = src;
	size_t n = size, dlen;

	while (n-- != 0 && *dst != '\0')
		dst++;
	dlen = dst - odst;
	n = size - dlen;

	if (n-- == 0)
		return dlen + strlen(src);
	while (*src != '\0') {
		if (n != 0) {
			*dst++ = *src;
			n--;
		}
		src++;
	}
	*dst = '\0';

	return dlen + (src - osrc);
}

size_t strlcpy(char *dst, const char *src, size_t size)
{
	const char *osrc = src;
	size_t n = size;

	if (n != 0)
		while (--n != 0)
			if ((*dst++ = *src++) == '\0')
				break;
	if (n == 0) {
		if (size != 0)
			*dst = '\0';
		while (*src++);
	}
	return src - osrc - 1;
}

int mkd(char *path)
{
	char *s, tmp[LINE_MAX];
	
	strlcpy(tmp, path, sizeof(tmp));
	for (s = tmp + 1; *s; s++) {
		if (*s == '/') {
			*s = '\0';
			if (mkdir(tmp, 0755) == -1 && errno != EEXIST)
				return -1;
			*s = '/';
		}
	}
	if (mkdir(path, 0755) == -1 && errno != EEXIST)
		return -1;
	return 0;
}

int rm(char *src, char *dst, struct stat *s)
{
	return S_ISDIR(s->st_mode) ? rmdir(src) : unlink(src);
}

int cp(char *src, char *dst, struct stat *s)
{
	char buf[LINE_MAX];
	int nread, err, sfd, dfd = -1;

	if (S_ISDIR(s->st_mode))
		return mkd(dst);
	if ((sfd = open(src, O_RDONLY)) == -1)
		return -1;
	if ((dfd = open(dst, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)) == -1)
		goto error;
	while ((nread = read(sfd, buf, LINE_MAX)) > 0)
		if (write(dfd, buf, nread) != nread)
			goto error;
	if (fchmod(dfd, s->st_mode) == -1 || (dfd = close(dfd)) == -1)
		goto error;
	close(sfd);
	return 0;

error:
	err = errno;
	close(sfd);
	if (dfd >= 0)
		close(dfd);
	unlink(dst);
	errno = err;
	return -1;
}

int fsrec(char *src, char *dst, int level, int depthfirst, int (*func)(char *, char *, struct stat *))
{
	DIR *dir;
	struct stat st;
	struct dirent *e;
	int err, i = strlen(src);
	char s[LINE_MAX], d[PATH_MAX];

	strlcpy(s, src, sizeof(s));
	strlcpy(d, dst, sizeof(d));
	
	if ((dir = opendir(s))) {
		if (!depthfirst && !level) {
			strlcat(d, s + srclen, sizeof(d));
			if (stat(s, &st) || (func)(s, d, &st))
				goto error;
			d[dstlen] = '\0';
		}
		while ((e = readdir(dir))) {
			if (e->d_name[0] == '.' && (e->d_name[1] == '\0' || (e->d_name[1] == '.' && e->d_name[2] == '\0')))
				continue;
			s[i] = '/';
			s[i+1] = '\0';
			strlcat(s, e->d_name, sizeof(s));
			strlcat(d, s + srclen, sizeof(d));
			if (stat(s, &st) || (!depthfirst && (func)(s, d, &st)))
				goto error;
			if ((S_ISDIR(st.st_mode) && fsrec(s, dst, level + 1, depthfirst, func)) || (depthfirst && (func)(s, d, &st)))
				goto error;
			d[dstlen] = '\0';
			s[i] = '\0';
		}
		if (depthfirst && !level) {
			strlcat(d, s + srclen, sizeof(d));
			if (stat(s, &st) || (func)(s, d, &st))
				goto error;
		}
		closedir(dir);
	}
	return 0;

error:
	err = errno;
	closedir(dir);
	errno = err;
	return -1;
}

int main(int argc, char *argv[])
{
	char *s, dstpath[LINE_MAX], srcpath[LINE_MAX];

	if (argc < 3) {
		fprintf(stderr, "usage: %s [source] [destination]\n", argv[0]);
		return 1;
	}

	strlcpy(srcpath, argv[1], sizeof(srcpath));
	strlcpy(dstpath, argv[2], sizeof(dstpath));

	if ((s = strrchr(srcpath, '/')) == NULL) {
		fprintf(stderr, "copying root is not supported!\n");
		return 1;
	}
	*s = '\0';
	srclen = strlen(srcpath); /* size of the path without file/dir name */
	*s = '/';
	dstlen = strlen(dstpath); /* size of the destination without new file/dir name */

	/* fprintf(stderr, "using copy\n\n"); */
	/* if (fsrec(srcpath, dstpath, 0, 0, cp)) */
	/* 	return 1; */
	fprintf(stderr, "\n\nusing move\n\n");
	if (fsrec(srcpath, dstpath, 0, 0, cp) || fsrec(srcpath, dstpath, 0, 1, rm))
		return 1;
	/* fprintf(stderr, "\n\nusing remove\n\n"); */
	/* if (fsrec(srcpath, dstpath, 0, 1, rm)) */
	/* 	return 1; */

	return 0;
}
