#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char *argv[])
{
	int i, j, l;
	char cmd[BUFSIZ];

	if (argc <= 1)
		return EXIT_FAILURE;

	sprintf(cmd, "%s", "rm -rf");
	for (i = 1, l = strlen(cmd); i < argc; i++) {
		cmd[l++] = ' ';
		cmd[l++] = '\'';
		for (j = 0; j < strlen(argv[i]); j++)
			cmd[l++] = argv[i][j];
		cmd[l++] = '\'';
	}
	cmd[l] = '\0';

	printf("%s\n", cmd);
	if ((system(cmd)) != 0)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
