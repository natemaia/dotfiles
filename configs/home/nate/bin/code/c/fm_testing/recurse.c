/*
* Recursively list a directory and display it in a tree form.
* This eagerly following sub-directories as they are encountered.
* Copyright© Nathaniel Maia - Oct 2019
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

int initial;

void recurse(const char *name)
{
	DIR *d;
	int len, level = 1, i;
	struct stat st;
	struct dirent *e;
	char tmp[1024], path[1024], *s = tmp, *ss;

	strcpy(path, name); /* build a new path for each file/dir */
	strcpy(tmp, name);  /* temp to intentionally mangle and never fix */
	len = strlen(path); /* preserve original path */

	if ((d = opendir(path)) != NULL) {
		ss = s + initial; /* pointer into path so we can count how many directories deep we are */
		while ((s = strrchr(ss, '/')) != NULL) {
			*s = '\0';
			level++;
		}
		while ((e = readdir(d)) != NULL) {
			if (strcmp(e->d_name, ".") == 0 || strcmp(e->d_name, "..") == 0) /* skip self and parent */
				continue;
			strcat(path, "/");
			strcat(path, e->d_name);
			if (!stat(path, &st)) {
				for (i = 0; i < level; i++)
					fprintf(stdout, "|  ");
				if (S_ISDIR(st.st_mode)) {
					fprintf(stdout, "\e[1m%s/\e[0m\n", e->d_name);
					recurse(path);
				} else {
					fprintf(stdout, "%s\n", e->d_name);
				}
			}
			path[len] = '\0'; /* preserve original path after appending to it */
		}
	} else {
		fprintf(stderr, "failed to open directory %s\n", path);
	}
}

int main(int argc, char *argv[])
{
	int i;

	if (argc < 2) {
		fprintf(stderr, "usage: %s [path]\n", argv[0]);
		return 1;
	}
	for (i = 1; i < argc; i++) {
		initial = strlen(argv[i]);
		fprintf(stdout, "\e[1m%s/\e[0m\n", argv[i]);
		recurse(argv[i]);
	}

	return 0;
}
