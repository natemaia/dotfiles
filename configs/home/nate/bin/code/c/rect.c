#include <stdio.h>
#include <stdlib.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define CLAMP(x, min, max) (MIN(MAX((x), (min)), (max)))

#define OVERLAP(a0, a1, b0, b1) (MIN(a0, a1) <= MAX(b0, b1) && MIN(b0, b1) <= MAX(a0, a1))
#define INRECT(x, y, w, h, ox, oy, ow, oh) (OVERLAP(x, x + w, ox, ox + ow) && OVERLAP(y, y + h, oy, oy + oh))

int main(int argc, char *argv[])
{
	int wx = 0, wy = 0, ww = 3840, wh = 2160;

	unsigned int i;
	static int index = 0;
	int tw = ww / 3, th = wh / 3;
	int q[][3] = {
		{ 1, wx + tw,       wy + th },
		{ 1, wx + (tw * 2), wy + th },
		{ 1, wx,            wy + th },
		{ 1, wx + tw,       wy, },
		{ 1, wx + (tw * 2), wy, },
		{ 1, wx,            wy, },
		{ 1, wx + tw,       wy + (2 * th) },
		{ 1, wx + (tw * 2), wy + (2 * th) },
		{ 1, wx,            wy + (2 * th) }
	};

	int c[][4] = {
		/* x,      y,    w,    h */
		{ 0,       0, 1280,  720 },
		{ 1400,  400, 1000, 1000 },
		{ 1846, 1200,  400,  300 },
		{ 1280,  720, 1280,  720 },
	};
	
	for (unsigned int j = 0; j < sizeof(c) / sizeof(*c); j++) {
		for (i = 0; i < sizeof(q) / sizeof(*q); i++) {
			int cx = c[j][0], cy = c[j][1], cw = c[j][2], ch = c[j][3];
			/* int cx = t->x + (t->w / 2), cy = t->y + (t->h / 2); */

			if (!q[i][0]) continue;
			if (INRECT(cx, cy, cw, ch, q[i][1], q[i][2], tw, th)) {
				fprintf(stderr, "c[%u] - (%d,%d %dx%d) is inside q[%u] - (%d,%d %dx%d)\n", j, cx, cy, cw, ch, i, q[i][1], q[i][2], tw, th);
				q[i][0] = 0;
				break;
			}

			/* if ((cx > q[i][1] && cy > q[i][2] && cx < q[i][1] + tw && cy < q[i][2] + th)) { */
			/* 	q[i][0] = 0; */
			/* 	break; */
			/* } */
		}
	}

	for (i = 0; i < sizeof(q) / sizeof(*q); i++)
		if (q[i][0]) break;
	if (i == sizeof(q) / sizeof(*q)) {
		i = index;
		index = (index + 1) % (sizeof(q) / sizeof(*q));
	}
	fprintf(stderr, "available rect - q[%u] - (%d,%d %dx%d)\n", i, q[i][1], q[i][2], tw, th);
	return 0;
}
