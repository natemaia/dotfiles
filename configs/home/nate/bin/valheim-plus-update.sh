#!/bin/bash

# bash script to update/install the valheim plus mod
# written by Nathaniel Maia - 2021


# path to the valheim installation
path="$HOME/.local/share/Steam/steamapps/common/Valheim"

# download filename
file="UnixServer.tar.gz"

# link to latest mod version
link="https://github.com/valheimPlus/ValheimPlus/releases/latest/download"


if curl -fSL "$link/$file" -o "$path/$file" && [[ -e "$path/$file" ]]; then
	cd "$path" || exit 1
	tar xvzf "$path/$file" && rm -rfv "${path:?}/$file"
fi
