----------------------------------------------------------------------
-- my init.lua for neovim (2024)
----------------------------------------------------------------------

-- install lazy.nvim if it's not already
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath, })
end
vim.opt.rtp:prepend(lazypath)


----------------------------------------------------------------------
-- vim settings
----------------------------------------------------------------------

-- leader should be mapped *before* plugins are loaded
vim.g.mapleader = " "

-- vim option settings (non-defaults)
vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.title = true
vim.opt.foldenable = false
vim.opt.foldmethod = "syntax"

vim.opt.linebreak = true
vim.opt.scrolloff = 8
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4

vim.opt.timeoutlen = 300

vim.opt.undolevels = 5000
vim.opt.undofile = true

vim.opt.confirm = true
vim.opt.modeline = true
vim.opt.showmode = false
vim.opt.smartcase = true
vim.opt.joinspaces = false
vim.opt.breakindent = true

vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.equalalways = false

-- vim.opt.smoothscroll = true

vim.opt.spell = true
vim.opt.complete:append("kspell")

vim.opt.signcolumn = "number"
vim.opt.virtualedit = "block"

vim.opt.matchpairs:append("<:>")
vim.opt.cinoptions:append({ ":0", "l1", "t0" })
vim.opt.clipboard:prepend({ "unnamed", "unnamedplus" })
vim.opt.shortmess:append({ a = true, A = true, c = true, w = true, s = true })

-- clean up netrw a bit
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
vim.g.netrw_browse_split = 0

-- one group to rule them all
local nate = vim.api.nvim_create_augroup("nate", { clear = true })

-- in a graphical session use fancy glyphs and cursorline
if vim.env.DISPLAY ~= nil or vim.env.DISPLAY ~= "" then
	vim.opt.list = true
	vim.opt.termguicolors = true
	vim.opt.showbreak = "↪ "
	vim.opt.fillchars = { diff = "⣿", vert = "┃" }
	vim.opt.listchars = { tab = "  ", extends = "❯", precedes = "❮" }
	vim.api.nvim_create_autocmd({ "InsertEnter", "InsertLeave" }, {
		group = nate,
		command = [[set cursorline!]]
	})
end

----------------------------------------------------------------------
-- auto commands
----------------------------------------------------------------------

vim.api.nvim_create_autocmd("BufReadPost", {
	group = nate,
	callback = function(event)
		local exclude = { "gitcommit" }
		local buf = event.buf
		if vim.tbl_contains(exclude, vim.bo[buf].filetype) or vim.b[buf].last_loc then
			return
		end
		vim.b[buf].last_loc = true
		local mark = vim.api.nvim_buf_get_mark(buf, '"')
		local lcount = vim.api.nvim_buf_line_count(buf)
		if mark[1] > 0 and mark[1] <= lcount then
			pcall(vim.api.nvim_win_set_cursor, 0, mark)
		end
	end,
})

vim.api.nvim_create_autocmd("VimResized", {
	group = nate,
	callback = function()
		local current_tab = vim.fn.tabpagenr()
		vim.cmd("tabdo wincmd =")
		vim.cmd("tabnext " .. current_tab)
	end,
})

vim.api.nvim_create_autocmd("FocusGained", {
	group = nate,
	command = "checktime",
})

vim.api.nvim_create_autocmd("FileChangedShellPost", {
	group = nate,
	callback = function()
		vim.notify("Changes loaded from source")
	end
})

----------------------------------------------------------------------
-- key remaps
----------------------------------------------------------------------

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv", { desc = "move visual line up" })
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv", { desc = "move visual line down" })

vim.keymap.set("n", "n", "nzzzv", { desc = "center cursor when searching" })
vim.keymap.set("n", "N", "Nzzzv", { desc = "center cursor when searching" })

vim.keymap.set("n", "<Tab>", "==1j", { desc = "apply indenting to the current line" })
vim.keymap.set("v", "<Tab>", "=gv", { desc = "apply indenting to the selection" })

vim.keymap.set({ "n", "v" }, "j", "v:count ? 'j' : 'gj'",
	{ expr = true, desc = "gj for wrapped lines with numbered jumps" })
vim.keymap.set({ "n", "v" }, "k", "v:count ? 'k' : 'gk'",
	{ expr = true, desc = "gk for wrapped lines with numbered jumps" })

-- interactively change (prompt) the current word or visual selection
vim.keymap.set("n", "<leader>cw",
	[[:%s/\<<C-r><C-w>\>//gIc<Left><Left><Left><Left>i<BS>]],
	{ desc = "apply indenting to the current line" })
vim.keymap.set("v", "<leader>cw",
	[["hy:let @h = escape(getreg('h'), '/\$^.*[]')<CR>:%s/<C-r>h//gIc<Left><Left><Left><Left>i<BS>]],
	{ desc = "apply indenting to the selection" })

-- non-interactively change (ALL) the current word or visual selection
vim.keymap.set("n", "<leader>sw",
	[[:%s/\<<C-r><C-w>\>/<C-r>/gI<Left><Left><Left>i<BS>]],
	{})
vim.keymap.set("v", "<leader>sw",
	[["hy:let @h = escape(getreg('h'), '/\$^.*[]')<CR>:%s/<C-r>h//gI<Left><Left><Left>i<BS>]],
	{})

vim.keymap.set("n", "<leader>ss",
	[[:let b:_p = getpos(".") | %s/\s\+$//e | call setpos('.', b:_p)<CR>]],
	{})

-- x is a black hole in normal and visual modes (won't overwrite system clipboard), use d instead
vim.keymap.set({ "n", "v" }, "x", [["_x]])

-- yank entire file
vim.keymap.set("n", "gyy", [[gg"+yG'']])

-- fix spelling at current word
vim.keymap.set("n", "zz", [[msz=1<CR><CR>`s]], { silent = true })

-- toggle folding
vim.keymap.set("n", "ZA", [[:set foldenable!<CR>]])

-- swap visual modes (block is just better)
vim.keymap.set("n", "v", "<C-v>")
vim.keymap.set("n", "<C-v>", "v")
vim.keymap.set("v", "v", "<C-v>")
vim.keymap.set("v", "<C-v>", "v")

-- paste in insert mode
vim.keymap.set("i", "<C-v>", "<C-r>*")

-- run the q macro with Q
vim.keymap.set("n", "Q", "@q")

vim.keymap.set("v", "<leader>gr", [["_dP]], { desc = "paste over selection without wiping \"" })

-- Y and D act like expected
vim.keymap.set("n", "Y", "y$")
vim.keymap.set("n", "D", "d$")

-- jump back up tag stack, opposite of <C-]> goto definition
vim.keymap.set("n", "<C-[>", "<C-t>")

-- window navigation (splits)
vim.keymap.set("n", "<C-j>", "<C-w>j", { silent = true })
vim.keymap.set("n", "<C-k>", "<C-w>k", { silent = true })
vim.keymap.set("n", "<C-h>", "<C-w>h", { silent = true })
vim.keymap.set("n", "<C-l>", "<C-w>l", { silent = true })

-- tab navigation
vim.keymap.set("n", "<leader>te", ":tabnew<CR>", { silent = true })
vim.keymap.set("n", "<leader>tn", ":tabnext<CR>", { silent = true })
vim.keymap.set("n", "<leader>tf", ":tabfirst<CR>", { silent = true })
vim.keymap.set("n", "<leader>tp", ":tabprevious<CR>", { silent = true })

-- built-in goto definition and go back (can be overwritten by lsp)
vim.keymap.set("n", "<leader>gd", "<C-]>", { remap = true })
vim.keymap.set("n", "<leader>gb", "<C-o>", { remap = true })

-- find merge conflict markers
vim.keymap.set("n", "<leader>fc", [[/\v^[<\|=>]{7}( .*\|$)<CR>]])

-- disable search highlighting
vim.keymap.set("n", "<BS>", [[:nohlsearch<CR>]], { silent = true })

-- quick fix list binds
vim.keymap.set("n", "<Leader>e", vim.diagnostic.open_float, { silent = true })
vim.keymap.set("n", "<Leader>p", vim.diagnostic.goto_prev, { silent = true })
vim.keymap.set("n", "<Leader>n", vim.diagnostic.goto_next, { silent = true })
vim.keymap.set("n", "<Leader>q", vim.diagnostic.setloclist, { silent = true })

----------------------------------------------------------------------
-- user commands
----------------------------------------------------------------------

-- write file as root (requires a bit of setup -- NOT SAFE)
vim.api.nvim_create_user_command("W", [[exec "silent w !sudo tee % >/dev/null" | edit!]], {})

----------------------------------------------------------------------
-- lazy.nvim plugin setup
----------------------------------------------------------------------

require("lazy").setup({
	-- classic tpope goodies
	"tpope/vim-repeat",
	"tpope/vim-surround",
	"tpope/vim-commentary",
	{
		"tpope/vim-fugitive",
		event = "VeryLazy",
		config = function()
			vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
			vim.api.nvim_create_autocmd("BufWinEnter", {
				group = vim.api.nvim_create_augroup("git_fugitive", { clear = true }),
				callback = function()
					if vim.bo.ft ~= "fugitive" then return end
					local bufnr = vim.api.nvim_get_current_buf()
					local opts = { buffer = bufnr, remap = false }
					vim.keymap.set("n", "<leader>p", function() vim.cmd.Git("push") end, opts)
					vim.keymap.set("n", "<leader>P", function() vim.cmd.Git({ "pull", "--rebase" }) end, opts)
				end,
			})
		end,
	},

	-- colorscheme
	{
		"https://bitbucket.org/natemaia/vim-jinx",
		lazy = false,
		priority = 1000,
		config = function()
			vim.g.jinx_theme = "midnight"
			vim.cmd([[colorscheme jinx]])
		end,
	},

	-- statusline
	{
		"nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		config = function()
			local od = require("lualine.themes.onedark")
			od.normal.c.bg = "#2A2A2F"
			require("lualine").setup({
				options = {
					theme = od,
					globalstatus = true,
				},
				sections = {
					lualine_x = { 'filetype' }
				}
			})
		end,
	},

	-- bufferline
	{
		'akinsho/bufferline.nvim',
		lazy = true,
		event = { 'BufAdd' },
		config = function ()
			vim.keymap.set("n", "gb", ":BufferLinePick<CR>", { silent = true })
			require('bufferline').setup({
				highlights = { fill = { bg = "#2A2A2F" } },
				options = { always_show_bufferline = false }
			})
		end
	},

	-- colorize hex/rgb values with <leader>tc
	{
		"norcalli/nvim-colorizer.lua",
		event = "VeryLazy",
		config = function()
			vim.keymap.set("n", "<leader>tc", ":ColorizerToggle<CR>")
		end,
	},

	-- visualize and manage the undo tree in vim with `<leader>u`
	{
		"mbbill/undotree",
		event = "VeryLazy",
		config = function()
			vim.g.undotree_SetFocusWhenToggle = 1
			vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
		end,
	},

	-- displays a popup with possible key bindings of the command you started typing.
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		config = function()
			require("which-key").setup({})
		end,
	},

	-- search files and buffers
	{
		'nvim-telescope/telescope.nvim',
		event = 'VeryLazy',
		dependencies = { 'nvim-lua/plenary.nvim' },
		opts = {},
		config = function ()
			local builtin = require('telescope.builtin')
			vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
			vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
			vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
			vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
		end
	},

	-- syntax highlighting and linting
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		dependencies = { "nvim-treesitter/playground", },
		config = function()
			vim.opt.foldmethod = "expr"
			vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
			require("nvim-treesitter.configs").setup({
				ensure_installed = {
					"c",
					"cpp",
					"make",
					"lua",
					"luap",
					"luadoc",
					"vim",
					"vimdoc",
					"bash",
					"regex",
					"gitcommit",
					"git_rebase",
					"latex",
					"sxhkdrc",
					"markdown",
					"markdown_inline",
				},
				modules = {},
				ignore_install = {},
				sync_install = false,
				auto_install = false,
				-- playground = { enable = true },
				indent = { enable = true },
				highlight = {
					enable = true,
					additional_vim_regex_highlighting = false, -- disable vim builtin highlighting
				},
			})
		end,
	},

	-- auto completion of paired characters (also ties into the nvim-cmp below)
	{
		"windwp/nvim-autopairs",
		lazy = true,
		event = { "InsertEnter" },
		config = function()
			local pair = require("nvim-autopairs")
			local rule = require("nvim-autopairs.rule")
			local cond = require("nvim-autopairs.conds")
			pair.setup({
				event = "InsertEnter",
				check_ts = true,
				map_bs = true,
				map_cr = true,
				enable_check_bracket_line = false,
				ignored_next_char = "[%w]", -- dont pair if the next char is a word char
				ts_config = { -- don't pair on these treesitter nodes
					c = { "string", "comment" },
					sh = { "string", "comment" },
					vim = { "string", "comment" },
					lua = { "string", "comment" },
				}
			})

			-- add space between brackets
			local brackets = { { "(", ")" }, { "[", "]" }, { "{", "}" } }
			for _, bracket in pairs(brackets) do
				pair.add_rules({rule(bracket[1] .. " ", " " .. bracket[2])
					:with_pair(cond.none())
					:with_move(function(opts) return opts.char == bracket[2] end)
					:with_del(cond.none())
					:use_key(bracket[2])
					:replace_map_cr(function(_) return "<C-c>2xi<CR><C-c>O" end)
				})
			end
			local function rule2(a1, ins, a2, lang)
				pair.add_rule(rule(ins, ins, lang)
					:with_pair(function(opts)
						return a1..a2 == opts.line:sub(opts.col - #a1, opts.col + #a2 - 1)
					end)
					:with_move(cond.none())
					:with_cr(cond.none())
					:with_del(function(opts)
						local col = vim.api.nvim_win_get_cursor(0)[2]
						return a1..ins..ins..a2 == opts.line:sub(col - #a1 - #ins + 1, col + #ins + #a2)
					end)
				)
			end
			rule2("(", " ", ")")
			rule2("{", " ", "}")
			rule2("[", " ", "]")
		end,
	},

	-- lsp setup for completion and linting
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"folke/neodev.nvim",
			"williamboman/mason.nvim",
			"williamboman/mason-lspconfig.nvim",
		},
		config = function()
			require("neodev").setup() -- neodev.setup must be called before lspconfig (through mason)
			require("mason").setup()
			require("mason-lspconfig").setup({
				ensure_installed = {
					"clangd",
					"bashls",
					"vimls",
					"lua_ls",
				},
				handlers = {
					function(server_name)
						require("lspconfig")[server_name].setup({
							-- additional capabilities provided by nvim-cmp
							capabilities = vim.tbl_deep_extend("force",
								require('lspconfig.util').default_config.capabilities,
								require("cmp_nvim_lsp").default_capabilities(),
								{}
						), })
					end
				},
			})

			-- options for vim.diagnostic.config()
			vim.diagnostic.config({
				severity_sort = true,
				update_in_insert = false,
				virtual_text = false,
				float = { border = "single" },
				inlay_hints = { enabled = false },
			})

			-- borders around lsp popup windows
			require("lspconfig.ui.windows").default_options = { border = "single" }
			vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
				vim.lsp.handlers.hover, { border = "single" })
			vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
				vim.lsp.handlers.signature_help, { border = "single" })

			-- per-buffer binds for lsp functionality
			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("lsp_setup", { clear = true }),
				callback = function(ev)
					local opts = { buffer = ev.buf }
					vim.keymap.set("n", "gb",		 "<C-o>",					   opts)
					vim.keymap.set("n", "K",		  vim.lsp.buf.hover,		   opts)
					vim.keymap.set("n", "gD",		  vim.lsp.buf.declaration,	   opts)
					vim.keymap.set("n", "gd",		  vim.lsp.buf.definition,	   opts)
					vim.keymap.set("n", "gr",		  vim.lsp.buf.references,	   opts)
					vim.keymap.set("n", "gi",		  vim.lsp.buf.implementation,  opts)
					vim.keymap.set("n", "gt",		  vim.lsp.buf.type_definition, opts)
					vim.keymap.set("n", "<C-k>",	  vim.lsp.buf.signature_help,  opts)
					vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename,		   opts)
				end,
			})
		end,
	},

	-- autocompletion plugin
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-nvim-lua",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-nvim-lsp-signature-help",
			"saadparwaiz1/cmp_luasnip",
			{ -- snippets
				"L3MON4D3/LuaSnip",
				build = "make install_jsregexp",
				opts = {
					history = true,
					delete_check_events = "TextChanged",
				},
				dependencies = {{
					"rafamadriz/friendly-snippets",
					config = function()
						require("luasnip.loaders.from_vscode").lazy_load()
					end,
				}},
			},
		},
		event = { "InsertEnter", "CmdlineEnter" },
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")

			local has_words_before = function()
				unpack = unpack or table.unpack
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			cmp.setup({
				window = { documentation = cmp.config.window.bordered({ border = "single" }) },
				snippet = { expand = function(args) luasnip.lsp_expand(args.body) end, },
				mapping = cmp.mapping.preset.insert({
					["<C-u>"] = cmp.mapping.scroll_docs(-4), -- Up
					["<C-d>"] = cmp.mapping.scroll_docs(4), -- Down
					["<Up>"] = cmp.mapping.select_prev_item(),
					["<Down>"] = cmp.mapping.select_next_item(),
					["<CR>"] = cmp.mapping({
						i = function(fallback)
							if cmp.visible() and cmp.get_active_entry() then
								cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
							else
								fallback()
							end
						end,
						s = cmp.mapping.confirm({ select = true }),
					}),
					["<Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						elseif has_words_before() then
							cmp.complete()
						elseif vim.api.nvim_get_mode().mode == "n" then
							vim.cmd("==1j")
						else
							fallback()
						end
						end, { "i", "s" }),
					["<S-Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif luasnip.jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
						end, { "i", "s" }),
				}),
				sources = cmp.config.sources({
					{ name = "nvim_lsp" },
					{ name = "nvim_lua" },
					{ name = "nvim_lsp_signature_help" },
					}, {
						{ name = "luasnip" },
						{ name = "buffer" },
						{ name = "path" },
				}),
			})

			-- insert pairs upon completion
			local cmp_autopairs = require("nvim-autopairs.completion.cmp")
			cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
		end,
	},

},
	{ -- configuration for lazy.nvim
		ui = { border = "single" },
		install = { colorscheme = { "jinx" } },
		change_detection = { notify = false },
		performance = {
			rtp = {
				disabled_plugins = {
					"gzip",
					"matchit",
					"tar",
					"tarPlugin",
					"tohtml",
					"tutor",
					"fzf",
					"zip",
					"zipPlugin",
					"netrw",
					"netrwPlugin",
				},
			},
		},
	}
)

