#!/bin/zsh

if [[ $- != *i* ]]; then
	return
elif [[ $PROFILE_ZSHRC ]]; then
	zmodload zsh/zprof
fi

SHELL=$(which zsh || echo '/bin/zsh')
KEYTIMEOUT=1
SAVEHIST=10000
HISTSIZE=10000
HISTFILE="$HOME/.cache/.zsh_history"
CDPATH='.:~:/media/wdblue/git/archlabs:/media/wdblue/git/personal:/media/wdblue'
SF_USERNAME="smokeking"
AL_PATH="/media/wdblue/git/archlabs/repos"

export EDITOR=nvim
export VISUAL=nvim

# settings for man and less
export MANWIDTH=120
export LESS='-R -x4'
export LESSHISTFILE=-  # no ~/.lesshist
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[32m'
export LESS_TERMCAP_mb=$'\e[31m'
export LESS_TERMCAP_md=$'\e[31m'
export LESS_TERMCAP_so=$'\e[47;30m'
export LESSPROMPT='?f%f .?ltLine %lt:?pt%pt\%:?btByte %bt:-...'

typeset -Ug fpath=($HOME/.zsh/fpath $fpath)
typeset -g  comppath="$HOME/.cache"
typeset -g  compfile="$comppath/.zcompdump"

[[ -w "$compfile" ]] || rm -rf "$compfile" >/dev/null 2>&1

if [[ $TERM == 'linux' ]]; then
	sudo kbdrate -r 70 -d 300 >/dev/null 2>&1
	col=("\e]P0111111" "\e]P1EE5555" "\e]P288BB88" "\e]P3FFCC66"  # 0-3
		 "\e]P46699CC" "\e]P5AA88CC" "\e]P655CCFF" "\e]P7CCCCCC"  # 4-7
		 "\e]P8222222" "\e]P9FF4444" "\e]PA66BB66" "\e]PBEE8844"  # 8-11
		 "\e]PC3388EE" "\e]PDDD88DD" "\e]PE44DDEE" "\e]PFFFFFFF") # 12-15
	printf '%b' "${col[@]}"
	printf '\033[40;1;37m\033[8]'
	clear
	unset col
fi

alias g='git'
alias v='nvim'
alias q='exit'
alias vi='nvim'
alias vim='nvim'
alias la='ls -Ah'
alias ll='ls -lAh'
alias lg='lazygit'
alias du='du -h'
alias df='df -h'
alias vd='nvim -d'
alias sv='sudo nvim'
alias sf='sudo noice'
alias sp='sudo pacman'
alias pls='pacman -Ql'
alias mkdir='mkdir -p'
alias grep='grep --color=auto'
alias hex2dec="printf '%d\n'"
alias dec2hex="printf '0x%x\n'"
alias bunny='printf "\n{\_/}\n(• •)\n/⊃ ⊃\n"'
alias calc='python -qi -c "from math import *"'
alias timer='time read -p "Press enter to stop"'
alias xeph='Xephyr -br -ac -noreset -screen 1920x1080 :1'
alias sx='startx -- -keeptty > ~/.local/share/xorg/Xorg.0.log 2>&1'
alias grabcol='grabpixel | xclip -i -selection clipboard'
alias broken='sudo find . -type l -! -exec test -e {} \; -print'
alias smk='make clean && make && sudo make install && make clean'
alias xpaste="xclip -i <<< \"\$(curl -ns -F 'f:1=<-' http://ix.io)\""
alias cam='ffplay -f video4linux2 -i /dev/video0 -video_size 720x720'
alias spkg='makepkg --printsrcinfo > .SRCINFO && makepkg -fsrc --sign'
alias xp='xprop | awk -F\"'" '/CLASS/ {printf \"NAME = %s\nCLASS = %s\n\", \$2, \$4}'"
alias gif='byzanz-record -x 1920 -w 1920 -y 0 -h 1080 -v -d 10 ~/Videos/$(date +%a-%d-%S).gif'
alias vid='ffmpeg -video_size 3840x2160 -framerate 60 -f x11grab -i :0.0 -c:v libx264 -qp 0 -preset ultrafast ~/Videos/$(date +%a-%d-%S).mp4'
alias vida='ffmpeg -video_size 3840x2160 -framerate 60 -f x11grab -f pulse -ac 2 -i :0.0 -c:v libx264 -c:a ac3 -ab 320k -qp 0 -preset ultrafast ~/Videos/$(date +%a-%d-%S).mp4'

# completion
setopt CORRECT
setopt NO_NOMATCH
setopt LIST_PACKED
setopt ALWAYS_TO_END
setopt GLOB_COMPLETE
setopt COMPLETE_ALIASES
setopt COMPLETE_IN_WORD
# builtin command behaviour
setopt AUTO_CD
setopt C_BASES
setopt BSD_ECHO
setopt OCTAL_ZEROES
# job control
setopt AUTO_CONTINUE
setopt LONG_LIST_JOBS
# history control
setopt HIST_VERIFY
setopt SHARE_HISTORY
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
# misc
setopt EXTENDED_GLOB
setopt TRANSIENT_RPROMPT
setopt INTERACTIVE_COMMENTS


f() # run noice
{
	noice "$@" # && builtin cd "$(< /tmp/noicedir)"
}

ls() # ls with preferred arguments
{
	command ls --color=auto -1F "$@"
}

cd() # cd and ls after
{
	builtin cd "$@" && command ls --color=auto -F
}

ga() # git add with preferred arguments
{
	git add "${1:-.}"
}

gr() # git rebase with preferred arguments
{
	git rebase -i HEAD~${1:-5}
}

src() # recompile completion and reload zsh
{
	autoload -U zrecompile
	rm -rf "$compfile"*
	compinit -u -d "$compfile"
	zrecompile -p "$compfile"
	exec zsh
}

prf() # profile zsh initialization time
{
	export PROFILE_ZSHRC=1
	[[ $1 =~ (-x|--xtrace) ]] && { shift; set -x; zsh "$@"; set +x; } || exec zsh "$@"
	unset PROFILE_ZSHRC
}

mir() # update pacman mirror list
{
	if hash reflector >/dev/null 2>&1; then
		sudo reflector --verbose --country Canada --country "United States" \
			--score 100 -l 50 -f 10 --sort rate --save /etc/pacman.d/mirrorlist
	elif hash rankmirrors >/dev/null 2>&1; then
		local s="https://www.archlinux.org/mirrorlist/?country=US&country=CA&use_mirror_status=on"
		su -c "curl -L '$s' | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 10 - > /etc/pacman.d/mirrorlist"
	else
		printf "this requires reflector or rankmirrors installed\n"; return 1
	fi
}

por() # remove orphaned packages
{
	local orphans=($(pacman -Qtdq 2>/dev/null))
	if (( ${#orphans[@]} == 0 )); then
		printf "System has no orphaned packages\n"
	else
		sudo pacman -Rns "${orphans[@]}"
	fi
}

pss() # search and install package
{
	(( $# == 1 )) || { printf "usage: pss <package_search>\n"; return 1; }

	local inpkgs=() pkgs=($(pacman -Ssq "$1"))

	for ((i=1; i<=${#pkgs[@]}; i++)); do
		printf " \e[1;34m%d " $i
		pacman -Ss "^${pkgs[i]}$"
	done

	printf "\n\nEnter package number(s) to install: "
	read id

	if [[ $id && $id =~ [0-9] ]]; then
		for i in $(printf "%s" "$id"); do
			case $i in
				''|*[!0-9]*) : ;;
				*) inpkgs+=("${pkgs[i]}") ;;
			esac
		done
		(( ${#inpkgs[@]} == 0 )) || sudo pacman -S "${inpkgs[@]}"
	fi
}

arc() # create and extract archives
{
	if [[ $# -eq 0 || $1 =~ (--help|-h) ]]; then
		printf "usage: arc [-e, --extract] <archive_name>\n"
		printf "       arc [-n, --new] <name.type> files...\n"
		printf "\n\nwhen first argument is an archive --ext is assumed\n"
		return 0
	fi

	(( $# >= 2 )) && { arg="$1"; shift; } || arg='-e'

	case "$arg" in
		-e|--extract)
			if [[ "$1" && -e "$1" ]]; then
				case "$1" in
					*.tbz2|*.tar.bz2) tar xvjf "$1" ;;
					*.tgz|*.tar.gz) tar xvzf "$1" ;;
					*.tar.xz) tar xpvf "$1" ;;
					*.tar) tar xvf "$1" ;;
					*.gz) gunzip "$1" ;;
					*.zip) unzip "$1" ;;
					*.bz2) bunzip2 "$1" ;;
					*.7zip) 7za e "$1" ;;
					*.rar) unrar x "$1" ;;
					*.deb) { ar p "$1" data.tar.xz | tar x; } || { ar p "$1" data.tar.gz | tar xz; } ;;
					*) printf "invalid archive for extraction: %s" "$1"; return 1
				esac
			else
				printf "invalid archive for extraction: %s" "$1"; return 1
			fi ;;
		-n|--new)
			case "$1" in
				*.tar.*)
					n="${1%.*}" e="${1#*.tar.}"; shift
					tar cvf "$n" "$@" || return 1
					case "$e" in
						xz) xz "$n" ;;
						gz) gzip -9r "$n" ;;
						bz2) bzip2 -9zv "$n" ;;
						*) printf "invalid extension after tar %s" "$e"; return 1
					esac ;;
				*.gz) shift; gzip -9rk "$@" ;;
				*.zip) zip -9r "$@" ;;
				*.7z) 7z a -mx9 "$@" ;;
				*) printf "invalid extension %s" "$1"; return 1
			esac ;;
		*) printf "invalid argument %s (use --help)" "$arg"; return 1
	esac
}

sens() # read sensor data from system, requires root
{
	ncores=$(awk 'BEGIN{ i = 0 } /cpu MHz/ { i++ } END{ print i / 2 }' /proc/cpuinfo)

	sudo watch -n 1 -t "sensors -A 'k10temp-pci-*' |
		awk '!/Chipset|Tsensor|k10temp/' &&
		awk -v j=$ncores 'BEGIN{ s = \"\"; i = 0} /cpu MHz/ {
			if (i == j || i == j / 2 || i == j + (j / 2))
				s=s\"\\n\"
			if (s != \"\" && i != j && i != j / 2 && i != j + (j / 2))
				s=s\" | \"int(\$4)\"MHz\"
			else
				s=s\"\"int(\$4)\"MHz\"
			i++
		} END{print s}' /proc/cpuinfo &&
		echo &&
		sensors -A 'amdgpu-pci-*' | awk '!/emerg|amdgpu|fan1/ {
				gsub(/\\s*\\(.*\\)$/, \"\")
				print
			}' &&
		awk '/MHz|%|GPU Load/ {
				sub(/^\s*/, \"\")
				print
			}' /sys/kernel/debug/dri/0/amdgpu_pm_info &&
		awk '/ram usage:/ {
				gsub(/ram usage:/, \"Vram Used: \")
				gsub(/,/, \"\")
				print \$4, \$5, \$6
			}' /sys/kernel/debug/dri/0/amdgpu_vram_mm &&
		echo &&
		cat /sys/class/drm/card0/device/pp_dpm_sclk &&
		echo &&
		cat /sys/class/drm/card0/device/pp_dpm_mclk"
}

synciso() # sync files to archlabs sourceforge and R2
{
	rsync -auvLPh -e ssh $HOME/Downloads/Archlabs/iso/* "$SF_USERNAME@frs.sourceforge.net:/home/frs/project/archlabs-linux-minimo/ArchLabsMinimo"
	rclone sync $HOME/Downloads/Archlabs/iso/ r2:archlabs/iso/ -P
}

signiso() # copy isos from build and sign them
{
	mkdir -p $HOME/Downloads/Archlabs/{iso,unsigned}
	cp -fv /media/wdblue/build/out/* $HOME/Downloads/Archlabs/unsigned/
	cd $HOME/Downloads/Archlabs/unsigned

	for f in ./*; do
		sha256sum $f > ${f}.sha256sum
		gpg --digest-algo SHA256 -a --clearsign ${f}.sha256sum
		rm -v ${f}.sha256sum
	done

	mv -fv $HOME/Downloads/Archlabs/unsigned/* $HOME/Downloads/Archlabs/iso/
}

upu() # update testing repos using their updaterepo.sh scripts
{
	[[ -x "$AL_PATH/archlabs-testing/x86_64/updaterepo.sh" ]] && "$AL_PATH/archlabs-testing/x86_64/updaterepo.sh"
	[[ -x "$AL_PATH/github/archlabs-testing/x86_64/updaterepo.sh" ]] && "$AL_PATH/github/archlabs-testing/x86_64/updaterepo.sh"
	[[ -x "$AL_PATH/non-git/archlabs-testing/x86_64/updaterepo.sh" ]] && "$AL_PATH/non-git/archlabs-testing/x86_64/updaterepo.sh"
}

upr() # update stable repos using their updaterepo.sh scripts
{
	[[ -x "$AL_PATH/archlabs/x86_64/updaterepo.sh" ]] && "$AL_PATH/archlabs/x86_64/updaterepo.sh" "$SF_USERNAME"
	[[ -x "$AL_PATH/github/archlabs/x86_64/updaterepo.sh" ]] && "$AL_PATH/github/archlabs/x86_64/updaterepo.sh"
	[[ -x "$AL_PATH/non-git/archlabs/x86_64/updaterepo.sh" ]] && "$AL_PATH/non-git/archlabs/x86_64/updaterepo.sh"
}

cpr() # add files ($@) to stable repos
{
	typeset -i u=0 j=0
	typeset -a bases
	typeset filepath=''

	if [[ $1 =~ (-h|--help) || $# -eq 0 ]]; then
		printf "usage: cpr [-u,--update] <file(s)>\n"
		return
	elif [[ $1 == '-u' || $1 == '--update' ]]; then
		u=1
		shift
	fi

	for filepath; do
		[[ -e $filepath ]] || { base=''; break; }
		base="${filepath##*/}"
		find "$AL_PATH/archlabs/x86_64/" -name "${base%%-[0-9]*[\-.][0-9]*}-[0-9]*.tar.*" -delete -printf "remove %p\n"
		find "$AL_PATH/github/archlabs/x86_64/" -name "${base%%-[0-9]*[\-.][0-9]*}-[0-9]*.tar.*" -delete -printf "remove %p\n"
		find "$AL_PATH/non-git/archlabs/x86_64/" -name "${base%%-[0-9]*[\-.][0-9]*}-[0-9]*.tar.*" -delete -printf "remove %p\n"
	done

	if [[ -z $base ]]; then
		printf "cpr: one or more of the given files does not exist\n"
		return 1
	fi

	command cp -fv "$@" "$AL_PATH/non-git/archlabs/x86_64/" || return
	command cp -fv "$@" "$AL_PATH/github/archlabs/x86_64/" || return
	command mv -fv "$@" "$AL_PATH/archlabs/x86_64/" || return
	(( ! u )) || upr
}

cpu() # add files ($@) to testing repos
{
	typeset -i u=0

	if [[ $1 =~ (-h|--help) || $# -eq 0 ]]; then
		printf "usage: cpu [-u,--update] <file(s)>\n"
		return
	elif [[ $1 =~ (-u|--update) ]]; then
		u=1
		shift
	fi

	for filepath; do
		[[ -e $filepath ]] || { base=''; break; }
		base="${filepath##*/}"
		find "$AL_PATH/archlabs-testing/x86_64/" -name "${base%%-[0-9]*[\-.][0-9]*}-[0-9]*.tar.*" -delete -printf "remove %p\n"
		find "$AL_PATH/github/archlabs-testing/x86_64/" -name "${base%%-[0-9]*[\-.][0-9]*}-[0-9]*.tar.*" -delete -printf "remove %p\n"
		find "$AL_PATH/non-git/archlabs-testing/x86_64/" -name "${base%%-[0-9]*[\-.][0-9]*}-[0-9]*.tar.*" -delete -printf "remove %p\n"
	done

	if [[ -z $base ]]; then
		printf "cpu: one or more of the given files does not exist\n"
		return 1
	fi

	command cp -fv "$@" "$AL_PATH/github/archlabs-testing/x86_64/" || return
	command cp -fv "$@" "$AL_PATH/non-git/archlabs-testing/x86_64/" || return
	command mv -fv "$@" "$AL_PATH/archlabs-testing/x86_64/" || return
	(( ! u )) || upu
}

surfs() # tabbed instance of surf with default pages
{
    if ! hash surf-open tabbed surf >/dev/null 2>&1; then
        local reqs="tabbed, surf, surf-open (shell script provided with surf)"
        printf "error: this requires the following installed\n\n\t%s\n" "$reqs"
        return 1
    fi

    declare -a urls
    if (( $# == 0 )); then
		local main="https://www.google.com"
		urls=("https://www.youtube.com"
			  "https://forum.archlabslinux.com"
			  "https://bitbucket.org"
			  "https://suckless.org"
		)
	else
		local main="$1"
        shift
        for arg in "$@"; do
            urls+=("$arg")
        done
    fi

    (
        surf-open "$main" &
        sleep 0.1
        for url in "${urls[@]}"; do
            surf-open "$url" &
        done
    ) & disown
}

tally_gst()
{
	if [[ $1 =~ (-h|--help) || $# -eq 0 ]]; then
		printf "usage: tally_gst <month range> [year]\n\n\te.g: tally_gst '5-8'\n"
		return
	fi
	
	range="$1"
	begr="${range%%-*}"
	endr="${range##*-}"
	shift

	if [[ $1 ]]; then
		year="$1.csv"
	else
		year="$(date +%Y.csv)"
	fi
	file="$HOME/Documents/invoices/$year"

	# build a regex string for awk to search from the range given e.g. 7-12 -> (07|08|09|10|11|12)
	regex='('
	for ((i = begr; i <= endr; i++)); do
		(( i < 10 )) && regex+="0"
		regex+="$i"
		(( i != endr )) && regex+="|"
	done
	regex+=")"

	echo
	echo "--------------------------------------------------------------------------------"
	echo "   Type    | Date    | Description                 |  Net   |  GST   |  Total"
	echo "--------------------------------------------------------------------------------"

	awk -F',' '
	BEGIN {
		net=0.0; gst=0.0; tot=0.0
	}
	/ '$regex'\// {
		net+=$4; gst+=$5; tot+=$6
		print "   "$0
	}
	END {
		while (i++ < 80) { printf "-" }
		printf "\n%51s %.2f   %.2f   %.2f\n\n", "=", net, gst, tot
		printf "%40s line 101 = %0.2f\n", "", tot
		printf "%40s line 105 = %0.2f\n\n", "", tot * 0.036
	}' "$file"
}


first_tab() # on first tab without any text it will list the current directory
{
	if [[ $#BUFFER == 0 ]]; then
		BUFFER="cd " CURSOR=3
		zle list-choices
		BUFFER="" CURSOR=1
	else
		zle expand-or-complete
	fi
}; zle -N first_tab

exp_alias() # expand aliases to the left (if any) before inserting a space
{
	zle _expand_alias
	zle self-insert
}; zle -N exp_alias

autoload -U compinit      # completion
autoload -U terminfo      # terminfo keys
autoload -U promptinit    # prompt
zmodload -i zsh/complist  # menu completion

# fix corrupt terminal
autoload -Uz add-zsh-hook
reset_broken_terminal() { printf '%b' '\e[0m\e(B\e)0\017\e[?5l\e7\e[0;0r\e8' }; add-zsh-hook -Uz precmd reset_broken_terminal

# better history navigation
autoload -U up-line-or-beginning-search; zle -N up-line-or-beginning-search
autoload -U down-line-or-beginning-search; zle -N down-line-or-beginning-search

# set the terminal mode when entering or exiting zle, otherwise terminfo keys are not loaded
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	zle-line-init() { echoti smkx; }; zle -N zle-line-init
	zle-line-finish() { echoti rmkx; }; zle -N zle-line-finish
fi

# stop '^Y' output on scroll-up in st terminal, this is dumb and you shouldn't use it
noop(){ :;}; zle -N noop
[[ $TERM =~ (st|st-255color) ]] && bindkey -- '^Y' noop

bindkey -- '^I'   first_tab
bindkey -- ' '    exp_alias
bindkey -- '^P'   up-history
bindkey -- '^N'   down-history
bindkey -- '^E'   end-of-line
bindkey -- '^A'   beginning-of-line
bindkey -- '^[^M' self-insert-unmeta # alt-enter to insert a newline/carriage return
bindkey -- '^K'   up-line-or-beginning-search
bindkey -- '^J'   down-line-or-beginning-search

[[ -n ${terminfo[kdch1]} ]] && bindkey -- "${terminfo[kdch1]}" delete-char                   # delete
[[ -n ${terminfo[kend]}  ]] && bindkey -- "${terminfo[kend]}"  end-of-line                   # end
[[ -n ${terminfo[kcuf1]} ]] && bindkey -- "${terminfo[kcuf1]}" forward-char                  # right arrow
[[ -n ${terminfo[kcub1]} ]] && bindkey -- "${terminfo[kcub1]}" backward-char                 # left arrow
[[ -n ${terminfo[kich1]} ]] && bindkey -- "${terminfo[kich1]}" overwrite-mode                # insert
[[ -n ${terminfo[khome]} ]] && bindkey -- "${terminfo[khome]}" beginning-of-line             # home
[[ -n ${terminfo[kbs]}   ]] && bindkey -- "${terminfo[kbs]}"   backward-delete-char          # backspace
[[ -n ${terminfo[kcbt]}  ]] && bindkey -- "${terminfo[kcbt]}"  reverse-menu-complete         # shift-tab
[[ -n ${terminfo[kcuu1]} ]] && bindkey -- "${terminfo[kcuu1]}" up-line-or-beginning-search   # up arrow
[[ -n ${terminfo[kcud1]} ]] && bindkey -- "${terminfo[kcud1]}" down-line-or-beginning-search # down arrow

# correction
zstyle ':completion:*:correct:*' original true
zstyle ':completion:*:correct:*' insert-unambiguous true
zstyle ':completion:*:approximate:*' max-errors 'reply=($(( ($#PREFIX + $#SUFFIX) / 3 )) numeric)'

# completion
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$comppath"
zstyle ':completion:*' rehash true
zstyle ':completion:*' verbose true
zstyle ':completion:*' insert-tab false
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:-command-:*:' verbose false
zstyle ':completion::complete:*' gain-privileges 1
zstyle ':completion:*:manuals.*' insert-sections true
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*' completer _complete _match _approximate _ignored
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories

# labels and categories
zstyle ':completion:*' group-name ''
zstyle ':completion:*:matches' group 'yes'
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:options' auto-description '%d'
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
zstyle ':completion:*' format ' %F{green}->%F{yellow} %d%f'
zstyle ':completion:*:messages' format ' %F{green}->%F{purple} %d%f'
zstyle ':completion:*:descriptions' format ' %F{green}->%F{yellow} %d%f'
zstyle ':completion:*:warnings' format ' %F{green}->%F{red} no matches%f'
zstyle ':completion:*:corrections' format ' %F{green}->%F{green} %d: %e%f'

# menu colours
eval "$(dircolors)"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=36=0=01'

# command parameters
zstyle ':completion:*:functions' ignored-patterns '(prompt*|_*|*precmd*|*preexec*)'
zstyle ':completion::*:(-command-|export):*' fake-parameters ${${${_comps[(I)-value-*]#*,}%%,*}:#-*-}
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm -w -w"
zstyle ':completion:*:processes-names' command 'ps c -u ${USER} -o command | uniq'
zstyle ':completion:*:(vim|nvim|vi):*' ignored-patterns '*.(wav|mp3|flac|ogg|mp4|avi|mkv|iso|so|o|7z|zip|tar|gz|bz2|rar|deb|pkg|gzip|pdf|png|jpeg|jpg|gif)'

# hostnames and addresses
zstyle ':completion:*:ssh:*' tag-order 'hosts:-host:host hosts:-domain:domain hosts:-ipaddr:ip\ address *'
zstyle ':completion:*:ssh:*' group-order users hosts-domain hosts-host users hosts-ipaddr
zstyle ':completion:*:(scp|rsync):*' tag-order 'hosts:-host:host hosts:-domain:domain hosts:-ipaddr:ip\ address *'
zstyle ':completion:*:(scp|rsync):*' group-order users files all-files hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-host' ignored-patterns '*(.|:)*' loopback ip6-loopback localhost ip6-localhost broadcasthost
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-domain' ignored-patterns '<->.<->.<->.<->' '^[-[:alnum:]]##(.[-[:alnum:]]##)##' '*@*'
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-ipaddr' ignored-patterns '^(<->.<->.<->.<->|(|::)([[:xdigit:].]##:(#c,2))##(|%*))' '127.0.0.<->' '255.255.255.255' '::1' 'fe80::*'
zstyle -e ':completion:*:hosts' hosts 'reply=( ${=${=${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) 2>/dev/null)"}%%[#| ]*}//\]:[0-9]*/ }//,/ }//\[/ } ${=${(f)"$(cat /etc/hosts(|)(N) <<(ypcat hosts 2>/dev/null))"}%%\#*} ${=${${${${(@M)${(f)"$(cat ~/.ssh/config 2>/dev/null)"}:#Host *}#Host }:#*\**}:#*\?*}})'
ttyctl -f

# initialize completion
compinit -d "$compfile"

# initialize prompt
promptinit; prompt simpl

[[ -z $PROFILE_ZSHRC ]] || zprof
