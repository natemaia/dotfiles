#!/bin/bash

# Simple prompt for bash
# Written by Nathaniel Maia, 2018

# the main deal, prompt_main() builds PS1 each time bash calls PROMPT_COMMAND
PROMPT_COMMAND='prompt_main'

# additional less commonly used prompts
PS2='==> '
PS3='choose: '
PS4='|${BASH_SOURCE} ${LINENO}${FUNCNAME[0]:+ ${FUNCNAME[0]}()}|  '

# add some handy history commands, see `history --help`
PROMPT_COMMAND+='; history -n; history -w; history -c; history -r'

# bash uses \[..\] to wrap text in the prompt which we don't want to count as
# part of the prompt to avoid cursor position issues using completion and history
R='\[\e[0m\]'       BOLD='\[\e[1m\]'  RED='\[\e[31m\]'     GREEN='\[\e[32m\]'
YELLOW='\[\e[33m\]' BLUE='\[\e[34m\]' MAGENTA='\[\e[35m\]' CYAN='\[\e[36m\]'

prompt_defaults()
{ # basic settings

    : "${PMT_PMT=\$}" # prompt symbol, a # when UID is 0, $ otherwise
    : "${PMT_USERFMT=""}" # user@host format string
    : "${PMT_NEWLINE=\n}" # newline char, empty for single line prompt
    : "${PMT_SHCOL="$BLUE"}" # color for subshell name
    : "${PMT_WRAPCOL="$MAGENTA"}" # color for matching wrap characters

    if (( EUID == 0 )); then # are we root
        : "${PMT_USERCOL="$RED"}"
        PMT_USERFMT="${PMT_USERFMT:- \u}"
    else
        : "${PMT_USERCOL="$CYAN"}"
    fi

    if [[ $PMT_NEWLINE ]]; then # prompt is multi line
        : "${PMT_LNBR1="┌"}"  # ┌ ┏ ╓ ╒
        : "${PMT_LNBR2="└"}"  # └ ┗ ╙ ╘
        : "${PMT_ARROW=">"}"  # ➜ ➤ ► ▻ ▸ ▹ ❯
    else
        : "${PMT_LNBR1=">"}"
    fi

    # color the wraps
    PMT_LNBR1="${PMT_WRAPCOL}${PMT_LNBR1}$R"
    PMT_LNBR2="${PMT_WRAPCOL}${PMT_LNBR2}$R"
    PMT_ARROW="${PMT_WRAPCOL}${PMT_ARROW}$R"

    # git settings
    : "${_GIT_PRE="("}"
    : "${_GIT_SEP="|"}"
    : "${_GIT_SUF=")"}"
    : "${_GIT_BRCH="${MAGENTA}"}"
    : "${_GIT_CNFL="${RED}x$R"}"
    : "${_GIT_CHGD="${BLUE}+$R"}"
    : "${_GIT_UNMG="${YELLOW}*$R"}"
    : "${_GIT_CLN="${GREEN}✔$R"}"

    if [[ $TERM =~ (linux|rxvt) ]]; then
        : "${_GIT_STGD="${RED}.$R"}"
        : "${_GIT_AHD="${GREEN}^$R"}"
        : "${_GIT_BHD="${RED}v$R"}"
        : "${_GIT_UNTR="${YELLOW}--$R"}"
        : "${_GIT_STSH="${BLUE}S$R"}"
    else
        : "${_GIT_STGD="${RED}•$R"}"
        : "${_GIT_AHD="${GREEN}↑$R"}" # ⇡
        : "${_GIT_BHD="${RED}↓$R"}"   # ⇣
        : "${_GIT_UNTR="${YELLOW}…$R"}"
        : "${_GIT_STSH="${BLUE}⚑$R"}"
    fi
}

prompt_main()
{ # print the main prompt
    ecode=$? # last command exit status
    local ps1="" g=""

    # set the terminal title for supporting terminals
    if [[ $TERM =~ (xterm|rxvt|st|alacritty|kitty) ]]; then
        ps1+="\[\033]0;${TERM/-256color/}: bash - \w\007\]"
    fi

    ps1+="${PMT_LNBR1}"
    (( ecode != 0 )) && ps1+=" ${RED}$ecode${R}"

    # working directory
    ps1+="${PMT_USERCOL}${PMT_USERFMT}${PMT_WRAPCOL} \w"

    # process parent
    cat /proc/$PPID/stat | while read l; do
        [[ $l != *'('*')'* ]] || { sl="${l#*\(}" sl="${sl%)*}"; }
    done
    [[ $TERM == *"$sl"* || $SHELL == *"$sl"* ]] || ps1+=" ${PMT_SHCOL}($sl)"

    # git status
    [[ $PWD =~ (boot|bin|etc|usr|dev|lib|proc|sys|var) || $PWD == "$HOME" ]] || g="$(prompt_gitstats)"
    [[ $g ]] && ps1+=" $g"

    # multi line stuff
    [[ $PMT_NEWLINE ]] && ps1+="${PMT_NEWLINE}${PMT_LNBR2}${PMT_ARROW}"

    # finishing bits of the prompt
    export PS1="$ps1 ${PMT_USERCOL}${PMT_PMT}${R} "
}

prompt_gitstats()
{ # print a string of git info (if any)
    git status --porcelain=v2 -b 2>/dev/null | (
        typeset c="$PWD" b='' d='' x='' y=''
        typeset -i H B G S C M A U
        while read l; do case "$l" in
            'u'*) (( U++ )) ;;
            '?'*) (( A++ )) ;;
            *'.head'*) b="${l##*.head }" ;;
            *'.ab'*) l="${l#*.ab +}" H="${l% -*}" B="${l#* -}" ;;
            *[1-2]*) x="${l:2:1}" y="${l:3:1}"
                if [[ $x$y =~ (AA|AU|DD|DU|UA|UD|UU) ]]; then
                    (( C++ ))
                else
                    [[ $x =~ [ACDMR] ]] && (( G++ ))
                    [[ $y =~ [CDMR]  ]] && (( M++ ))
                fi ;;
        esac done
        if [[ $b ]]; then
            while [[ $c && -z $d ]]; do
                [[ -d "$c/.git" ]] && d="$c/.git" || c="${c%/*}"
            done
            if [[ -f $d/refs/stash ]]; then
                cat "$d/refs/stash" | while read l; do
                    (( S++ ))
                done
            fi
            s="${R}${_GIT_PRE}${_GIT_BRCH}${b}$R"
            (( H )) && s+="$_GIT_AHD${H}";  (( B )) && s+="$_GIT_BHD${B}"; s+="$_GIT_SEP"
            (( G )) && s+="$_GIT_STGD${G}"; (( M )) && s+="$_GIT_CHGD${M}"
            (( C )) && s+="$_GIT_CNFL${C}"; (( U )) && s+="$_GIT_UNMG${U}"
            (( A )) && s+="$_GIT_UNTR${A}"; (( ! M && ! C && ! G && ! A && ! U )) && s+="$_GIT_CLN"
            (( S )) && s+="$_GIT_STSH${S}"
            printf "%s" "${s}${_GIT_SUF}${R}"
        fi
    )
}

# always enable the promptvars option if not already
shopt -q promptvars || shopt promptvars >/dev/null 2>&1

prompt_defaults

# vim:ft=sh:fdm=marker:fmr={,}
