#!/bin/zsh

# set $ZDOTDIR to keep $HOME a bit less messy
typeset -gx ZDOTDIR="$HOME/.zsh"

# for performance metrics, set the launch options for steam games
# DXVK_HUD=full %command%

# debugging - https://wiki.archlinux.org/index.php/Core_dump
#   ulimit -c unlimited
#
# later in the shell
#   coredumpctl list
#   coredumpctl gdb MATCH
#   (gdb) bt

# add ~/.local/bin to the path
[[ $PATH =~ $HOME/.local/bin: ]] || export PATH=$HOME/.local/bin:$PATH

# environment
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_DIR="$HOME/.local/share"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"

# change tty colours
typeset -a col=(
	"\e]P0111111" "\e]P1EE5555" "\e]P288BB88" "\e]P3FFCC66" # 0-3
	"\e]P46699CC" "\e]P5AA88CC" "\e]P655CCFF" "\e]P7CCCCCC" # 4-7
	"\e]P8222222" "\e]P9FF4444" "\e]PA66BB66" "\e]PBEE8844" # 8-11
	"\e]PC3388EE" "\e]PDDD88DD" "\e]PE44DDEE" "\e]PFFFFFFF" # 12-15
)
printf '%b' "${col[@]}"
printf '\033[40;1;37m\033[8]'
clear
unset col

# recompile zshrc if it's changed
[ ! -f "$ZDOTDIR/.zshrc.zwc" -o "$ZDOTDIR/.zshrc" -nt "$ZDOTDIR/.zshrc.zwc" ] && zcompile "$ZDOTDIR/.zshrc"

# startx when logging in on tty1
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -keeptty > ~/.local/share/xorg/Xorg.0.log 2>&1
